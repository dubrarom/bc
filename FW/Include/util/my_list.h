/* List header file. */
#ifndef MY_LIST
#define MY_LIST

#define MIN_LIST_VALUE -1  /* All list values must be strictly greater then this. */

struct list_item{
	struct list_item* next_item;
	int value;
};

typedef struct list_item *sorted_list;


struct lex_node{
	int value;
	struct lex_node *next_alternative;
	struct lex_node	*next_item;
};



int ListFind(int Value, sorted_list list);
void ListAdd(int value, sorted_list *list);
int ListRemove(int value, sorted_list *list);
void ListFree(sorted_list list);
sorted_list ListCopy( sorted_list list);
void ListMerge( sorted_list list1, sorted_list *list2);
sorted_list ListIntersect(sorted_list list1, sorted_list list2);
sorted_list ListDifference(sorted_list list1, sorted_list list2);
int ListsAreEqual(sorted_list list1, sorted_list list2);
int ListSize(sorted_list list);
void PrintList(sorted_list list, FILE *fp);


struct lex_node *LexAdd(struct lex_node *Lex, struct list_item *List);
int LexInclude(struct lex_node* Lex, struct list_item *List);
void LexPrint(struct lex_node* Lex, sorted_list List, FILE *fp);
void LexFree(struct lex_node *Lex);


#endif