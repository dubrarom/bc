#ifndef MY_TYPES
#define MY_TYPES

typedef double my_float;  /* make sure OFScale in ob.c, ob_costs.c, ob_flows.c are defined as double. */
#define FLOAT_ACCURACY 1.0E-15

#define NO_COSTPARAMETERS 4
#define IVTT 0
#define OVTT 1
#define MONETARY 2
#define DIST 3

typedef my_float cost_vector[NO_COSTPARAMETERS];


#define TIME_FORMAT	"%.3f \t"
#define OF_FORMAT	"%.20g \t"
#define FLOAT_FORMAT	"%g \t"


#endif
