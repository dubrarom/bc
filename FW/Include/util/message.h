void StatusMessage(const char *group, const char *format, ...);
void Warning(const char *format, ...);
void ExitMessage(const char *format, ...);
void InputWarning(const char *format, ...);
void FatalInputError(FILE *fp, const char *FileName, const char *message_format, ...);

