void tuiGetInt(char *header, int mandatory, int *value);
void tuiGetDouble(char *header, int mandatory, double *value);
void tuiGetDoubleVector(char *header, int size, int mandatory, double *vector);
void tuiGetString(char *header, int mandatory, char **string);
void tuiGetStringVector(char *header, int size, int mandatory, char **string);
void tuiGetInputFileName(char *header, int mandatory, char **Name);
void tuiGetOutputFileName(char *header, int mandatory, char **Name);

void tuiInit(char *tuiFileName);
void tuiClose(char *tuiFileName);


