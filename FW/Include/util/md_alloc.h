
void *Alloc_1D(int dim1, size_t size);
void **Alloc_2D(int dim1, int dim2, size_t size);
void ***Alloc_3D(int dim1, int dim2, int dim3,  size_t size);
void ****Alloc_4D(int dim1, int dim2, int dim3, int dim4, size_t size);

void Free_2D(void **Array, int dim1, int dim2) ;
void Free_3D(void ***Array, int dim1, int dim2, int dim3) ;
void Free_4D(void ****Array, int dim1, int dim2, int dim3, int dim4) ;

