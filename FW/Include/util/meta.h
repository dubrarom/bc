#ifndef META
#define META

#define END_OF_META "END OF METADATA"


struct meta_struct{
	char *FileName;
	int no_items;	
	char **title, **value;
};



void GetMetaInt(char *title, int mandatory, int *i, struct meta_struct meta_data);
void GetMetaDouble(char *title, int mandatory, double *d, struct meta_struct meta_data);
void GetMetaString(char *title, int mandatory, char **string, struct meta_struct meta_data);

void ReadMeta(FILE *fp, struct meta_struct *meta_data) ;
void FreeMeta( struct meta_struct meta_data );


#endif


