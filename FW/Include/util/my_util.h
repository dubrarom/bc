#ifndef UTIL
#define UTIL

#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif


/*Min and Max macros, use only with values, not expressions,
as they will be evaluated twice. */

#define MIN(x,y)		( ((x)<(y)) ? (x) : (y) )
#define MAX(x,y)		( ((x)>(y)) ? (x) : (y) )
#define SQR(x) ((x)*(x))
#define FABS(x) ((x)>=0 ? (x) : (-x))

#define INVALID -1
#define VALID(x) ((x)!=-1)

char *Date_and_Time(void);
double my_clock(void);

FILE* FileOpen  ( const char *Filename , char *Mode);
void make_directory( char *DirectoryName );
void my_gets(FILE* file, char* Buffer, int size);
void SkipComments(FILE *fp);

void ReadValue( FILE* file, char *format, void *Variable, const char* Path );

void swap(int A[], int i, int j);
void quick_sort(int list[], int left, int right);

void check7(int x);


#endif
