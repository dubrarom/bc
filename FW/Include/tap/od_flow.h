my_float **Read_ODflow (char *ODflowFileName, my_float *TotalODflow, int *no_zones, struct meta_struct *meta_data);
my_float **Read_ODflow_colomns (char *ODflowFileName, my_float *TotalODflow, int *no_zones, struct meta_struct *meta_data);
void Write_ODflow (my_float **ODflow, int no_zones, char *ODflowFileName, const char *metadataformat, ...);

my_float TripTableTotalCost(my_float **Trips,my_float **Cost, int no_zones);


void PrintODflowExponentDistribution (my_float **ODflow, int no_zones, int MinExponent, int MaxExponent, FILE *StatFile);
void PrintODflowDistribution (my_float **ODflow, int no_zones, my_float Step, int no_cells, FILE *StatFile);

