void Clear_ODtable (my_float **ODtable, int no_zones) ;
void Factor_ODtable (my_float **ODtable, my_float Factor, int no_zones) ;
my_float Sum_ODtable (my_float **ODtable, int no_zones);
my_float SumAbs_ODtable (my_float **ODtable, int no_zones);

void Read_ODtable (FILE *ODtableFile, char *ODtableFileName, my_float **ODtable, int no_zones);
void Read_ODtable_colomns (FILE *ODtableFile, char *ODtableFileName, my_float **ODtable, int no_zones);
void Write_ODtable (my_float **ODtable, FILE *ODtableFile, int no_zones);
void Write_ODtable_colomns (my_float **ODtable, char *OutputFileName, int no_zones);


void PrintODtableExponentDistributionHeader (int MinExponent, int MaxExponent, FILE *StatFile);
void PrintODtableExponentDistribution (my_float **ODtable, int no_zones, int MinExponent, int MaxExponent, FILE *StatFile);
void PrintODtableDistributionHeader (my_float Step, int no_cells, FILE *StatFile) ;
void PrintODtableDistribution (my_float **ODflow, int no_zones, my_float Step, int no_cells, FILE *StatFile);

void ODtableFreqDist_Vert(my_float **Weight, int no_zones, int **Value, int max_value, char* ValueName, char* Title, FILE* FreqDistFile);
void ODtableFreqDist(my_float **Weight, int no_zones, int **Value, int max_value, char* Title, FILE* FreqDistFile);
void PrintFreqDistHeader(FILE *FreqDistFile, int max_value, char* ValueName);

