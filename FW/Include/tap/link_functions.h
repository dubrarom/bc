#ifndef LINK_FUNCTIONS
#define LINK_FUNCTIONS

#ifndef MY_TYPES
Must include my_types.h first
#endif


/* LINK VECTOR FUNCTIONS */
void ClearVolume ( my_float *VolumeArray );
void VolumeDifference( my_float *Volume1, my_float *Volume2, my_float *Difference);
void UpdateVolume ( my_float *MainVolume, my_float *SubVolume, my_float Lambda );

/* LINK COST FUNCTIONS */
void UpdateLinkAdditionalCost(void);
void UpdateLinkCost ( my_float *Volume);
void UpdateLinkCostDer (my_float *Volume);
void GetLinkTravelTimes(my_float *Volume, my_float *TravelTime);
my_float TotalLinkCost(my_float *Volume);

/*  LINK OBJECTIVE FUNCTION */
my_float OF_Links(my_float *MainVolume);
my_float OF_LinksDirectionalDerivative(my_float *MainVolume, my_float *SDVolume, my_float Lambda );



void GetLinkParameters(my_float *FreeFlowTravelTime, my_float *Capacity, my_float *Power, my_float *B_factor);
void SetLinkParameters(my_float *FreeFlowTravelTime, my_float *Capacity, my_float Power, my_float B_factor);

/* PRINT FUNCTIONS */
/*
void PrintVolumeInColomns(my_float* Volume, FILE* fp);
void PrintTailsInRow(FILE *fp);
void PrintHeadsInRow(FILE *fp);
void PrintVolumeInRow(my_float *Volume, FILE *fp);
*/
int CongestedLinks(my_float* Volume);
void ReportInterLinkFlows(my_float *Volume, int Iteration);
void ReportFinalLinkFlows(my_float *Volume);

void LoadLinkFlowsCosts(char *LoadLinkFlowsFileName, my_float *Volume, int SetCosts);
/* If Volume!=NULL, get link flows. If SetCosts is TRUE, set costs. */ 

void InitLinks(void);
void CloseLinks(void);

#endif
