int Minpath(int Orig , int *PredLink, my_float *Cost_to );
void FindMinCostRoutes(int **MinPathPredLink, my_float **RouteCost) ;
void Assign(my_float **ODflow, int **MinPathPredLink, my_float *Volume );
void AssignOrigin(int Orig, my_float *DestFlow, int *PredLink, my_float *Volume );
void RouteCostComponents(int **MinPathPredLink, cost_vector **RouteCosts) ;
my_float AverageExcessCost(my_float **ODflow, my_float ODflowTotal, my_float *MainVolume); 

