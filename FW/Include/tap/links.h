#ifndef LINKS
#define LINKS

#ifndef MY_TYPES
Must include my_types.h first
#endif

#include "my_list.h"

struct link_record{
	int Tail;
	int Head;
	my_float Capacity;
	my_float FreeTravelTime;
	my_float B;
	my_float BoverC;
	my_float Power;
	my_float Toll;
	my_float AdditionalCost;
	my_float Distance;
	my_float Speed;
	int Type;
	my_float Delay;
	my_float GenCost;
	my_float GenCostDer;
};

extern struct link_record *Link;
extern int *FirstLinkFrom;
extern int *LastLinkFrom;
extern sorted_list* LinksTo;

my_float Link_GenCost(int k, my_float *Volume);
my_float LinkCost_Integral(int k, my_float *Volume);
my_float Link_GenCostDer(int k, my_float *Volume);

#endif
