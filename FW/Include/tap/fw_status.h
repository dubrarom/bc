#ifndef FW_STATUS
#define FW_STATUS

struct fw_status_struct{
	int Iteration;
	double OF;
	double LowerBound;
	double BestLowerBound;
	double Gap;
	double RelativeGap;
	double AverageExcessCost;
	int LineSearchIterations;
};
	

int ContinueFW(struct fw_status_struct fw_status);
void InitFWstatus(struct fw_status_struct *fw_status);
void FirstFWstatus(my_float *MainVolume, struct fw_status_struct *fw_status);
void UpdateFWstatus(my_float *MainVolume, my_float *SDVolume, struct fw_status_struct *fw_status);
void CloseFWstatus(my_float *MainVolume);

#endif
