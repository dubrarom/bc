#include "stdafx.h"

#include <stdio.h> 	/* FILE */
#include <stdarg.h>	/* va_list, va_start, va_end, vprintf */
#include <math.h>	/* fabs, ceil, frexp, ldexp */

#include "os.h"
#include "my_util.h"
#include "md_alloc.h"
#include "meta.h"
#include "message.h"
#include "my_types.h"
#include "od_table.h"
#include "od_flow.h"

my_float **Read_ODflow (char *ODflowFileName, my_float *TotalODflow, int *no_zones, struct meta_struct *meta_data){
	FILE *ODflowFile;
	my_float **ODflow;
	double RealTotal, InputTotal;

	ODflowFile = FileOpen ( ODflowFileName , "r" );
	(*meta_data).FileName=ODflowFileName;
	ReadMeta( ODflowFile, meta_data);
	GetMetaInt("NUMBER OF ZONES", TRUE, no_zones, *meta_data);
	GetMetaDouble("TOTAL OD FLOW", TRUE, &InputTotal, *meta_data);
	if(InputTotal<0.0)
		ExitMessage("OD flow file '%s' total trips is negative %lf.", ODflowFileName , InputTotal);
	ODflow = (my_float**) Alloc_2D(*no_zones, *no_zones, sizeof(my_float) );
	Read_ODtable (ODflowFile, ODflowFileName, ODflow, *no_zones);
	fclose(ODflowFile);

	RealTotal = (double) Sum_ODtable(ODflow, *no_zones);
	if( fabs( RealTotal - InputTotal )  >  InputTotal * (*no_zones) * (*no_zones) * FLOAT_ACCURACY )
		InputWarning("in file '%s'\nActual total OD flow is %.15f; while the stated total is %.15f; " 
			"Relative error %g.", ODflowFileName, RealTotal, InputTotal, (RealTotal/InputTotal - 1.0));

	*TotalODflow = (my_float) RealTotal;	

	return(ODflow);
}



my_float **Read_ODflow_colomns (char *ODflowFileName, my_float *TotalODflow, int *no_zones, struct meta_struct *meta_data){
	FILE *ODflowFile;
	my_float **ODflow;
	double RealTotal, InputTotal;

	ODflowFile = FileOpen ( ODflowFileName , "r" );
	(*meta_data).FileName=ODflowFileName;
	ReadMeta( ODflowFile, meta_data);
	GetMetaInt("NUMBER OF ZONES", TRUE, no_zones, *meta_data);
	GetMetaDouble("TOTAL OD FLOW", TRUE, &InputTotal, *meta_data);
	if(InputTotal<0.0)
		ExitMessage("OD flow file '%s' total trips is negative %lf.", ODflowFileName , InputTotal);
	ODflow = (my_float**) Alloc_2D(*no_zones, *no_zones, sizeof(my_float) );
	Read_ODtable_colomns (ODflowFile, ODflowFileName, ODflow, *no_zones);
	fclose(ODflowFile);

	RealTotal = (double) Sum_ODtable(ODflow, *no_zones);
	if( fabs( RealTotal/InputTotal - 1.0 )  >  (*no_zones) * FLOAT_ACCURACY )
		InputWarning("in file '%s'\nActual total OD flow is %.15f; while the stated total is %.15f; " 
			"Relative error %g.", ODflowFileName, RealTotal, InputTotal, (RealTotal/InputTotal - 1.0));

	*TotalODflow = (my_float) RealTotal;	

	return(ODflow);
}



void Write_ODflow (my_float **ODflow, int no_zones, char *ODflowFileName, const char *metadataformat, ...){
	FILE *ODflowFile;
	double TotalODflow;
	va_list ap;
	va_start(ap, metadataformat);

	TotalODflow = (double) Sum_ODtable(ODflow, no_zones);

	ODflowFile = FileOpen ( ODflowFileName , "w" );
	vfprintf( ODflowFile, metadataformat, ap );
	fprintf(ODflowFile, "<NUMBER OF ZONES> %d \n", no_zones);
	fprintf(ODflowFile, "<TOTAL OD FLOW> %.20g \n", TotalODflow);
	fprintf(ODflowFile, "<%s> \n", END_OF_META);
	Write_ODtable (ODflow, ODflowFile, no_zones);
	fclose(ODflowFile);

	va_end(ap);
}







/* TripTableTotalCost -  total genralized cost from the given trip table and the given cost table. */
my_float TripTableTotalCost(my_float **Trips,my_float **Cost, int no_zones) {
	int Orig,Dest;
	my_float sum=0.0;
	my_float *Trips_Orig, *Cost_Orig;

	for ( Orig=1; Orig<=no_zones ; Orig++) {
		Trips_Orig=Trips[Orig];
		Cost_Orig=Cost[Orig];
		for ( Dest=1; Dest<=no_zones ; Dest++)	{
			sum+=Trips_Orig[Dest]*Cost_Orig[Dest];
		}
	}

	return sum;
}



void PrintODflowExponentDistribution (my_float **ODflow, int no_zones, int MinExponent, int MaxExponent, FILE *StatFile) {
	int Orig, Dest, Exponent, no_cells, *DistODpairs, ZeroODpairs, SubMinODpairs, OverMaxODpairs, CumODpairs;
	my_float Flow, FlowFraction, MaxFlow, *DistODflow, SubMinODflow, OverMaxODflow, CumODflow;

/* Initialize variables */
	no_cells = MaxExponent -  MinExponent +1;
	DistODpairs = (int *) Alloc_1D(no_cells, sizeof(int));
	DistODflow = (my_float *) Alloc_1D(no_cells, sizeof(my_float));
	for(Exponent=0;Exponent<no_cells;Exponent++) {
		DistODpairs[Exponent]=0;
		DistODflow[Exponent]=0.0;
	}
	ZeroODpairs = SubMinODpairs = OverMaxODpairs = 0;
	MaxFlow = SubMinODflow = OverMaxODflow = 0.0;

/* Compute distributions */
	for(Orig=1;Orig<=no_zones;Orig++) {
		for(Dest=1;Dest<=no_zones;Dest++) {
			Flow = ODflow[Orig][Dest];
			if(Flow < 0.0)
				ExitMessage("Can not compute OD flow exponent distribution with negative "
					"flow %g from origin %d to destination %d.", Flow, Orig, Dest);
			if(Flow==0.0){
				ZeroODpairs++;				
				continue;
			}
			FlowFraction = frexp (Flow, &Exponent); /* Find the exponent of the value. */
			if (FlowFraction==0.5) Exponent--;	/* Fix the rounding problem. */
			if (Exponent<=MinExponent){
				SubMinODpairs++; 
				SubMinODflow+=Flow;
			}
			else if (Exponent>MaxExponent){
				OverMaxODpairs++; 
				OverMaxODflow+=Flow;
				if(Flow>MaxFlow) MaxFlow=Flow;
			}
			else {
				DistODpairs[Exponent-MinExponent]++;
				DistODflow[Exponent-MinExponent]+=Flow;
			} 
		}
	}
	fprintf(StatFile, "Exponent distribution \n");
	fprintf(StatFile, "OD flow <= \tCummulative OD pairs\tCummulative flow \n");
	fprintf(StatFile, "0 \t%d \t0 \n", ZeroODpairs); 
	CumODpairs=ZeroODpairs+SubMinODpairs;
	CumODflow=SubMinODflow;
	fprintf(StatFile, "%g \t%d \t%g \n", ldexp(1.0,MinExponent), CumODpairs, (double)CumODflow); 
	for(Exponent=0;Exponent<no_cells;Exponent++) {
		CumODpairs+=DistODpairs[Exponent];
		CumODflow+=DistODflow[Exponent];
		fprintf(StatFile, "%g \t%d \t%g \n", ldexp(1.0,Exponent+MinExponent), CumODpairs, (double)CumODflow);
	}
	if(OverMaxODpairs>0){
		CumODpairs+=OverMaxODpairs;
		CumODflow+=OverMaxODflow;
		fprintf(StatFile, "%g \t%d \t%g \n", MaxFlow, CumODpairs, (double)CumODflow);
	}

	fprintf(StatFile, "\n\n");

}




void PrintODflowDistribution (my_float **ODflow, int no_zones, my_float Step, int no_cells, FILE *StatFile) {
	int Orig, Dest, FlowCell, LastCell, *DistODpairs, OverMaxODpairs, CumODpairs;
	my_float Flow, MaxFlow, *DistODflow, OverMaxODflow, CumODflow;

/* Initialize variables */
	DistODpairs = (int *) Alloc_1D(no_cells+1, sizeof(int));
	DistODflow = (my_float *) Alloc_1D(no_cells+1, sizeof(my_float));
	for(FlowCell=0;FlowCell<=no_cells;FlowCell++) {
		DistODpairs[FlowCell]=0;
		DistODflow[FlowCell]=0.0;
	}
	OverMaxODpairs = 0;
	MaxFlow = OverMaxODflow = 0.0;

/* Compute distributions */
	for(Orig=1;Orig<=no_zones;Orig++) {
		for(Dest=1;Dest<=no_zones;Dest++) {
			Flow = ODflow[Orig][Dest];
			if(Flow < 0.0)
				ExitMessage("Can not compute OD flow distribution with negative "
					"flow %g from origin %d to destination %d.", Flow, Orig, Dest);
			FlowCell = (int) ceil(Flow/Step);
			if (FlowCell>no_cells){
				OverMaxODpairs++; 
				OverMaxODflow+=Flow;
				if(Flow>MaxFlow) MaxFlow=Flow;
			}
			else {
				DistODpairs[FlowCell]++;
				DistODflow[FlowCell]+=Flow;
			}
		}
	}

/* Find the last non-empty cell. */
	for(LastCell=no_cells;DistODpairs[LastCell]==0;LastCell--);

	fprintf(StatFile, "Linear distribution \n");	
	fprintf(StatFile, "OD flow <= \tCummulative OD pairs\tCummulative flow \n");
	CumODpairs=0;
	CumODflow=0.0;
	for(FlowCell=0;FlowCell<=LastCell;FlowCell++) {
		CumODpairs+=DistODpairs[FlowCell];
		CumODflow+=DistODflow[FlowCell];
		fprintf(StatFile, "%g \t%d \t%g \n", (double)FlowCell*Step, CumODpairs, (double)CumODflow);
	}
	if(OverMaxODpairs>0){
		CumODpairs+=OverMaxODpairs;
		CumODflow+=OverMaxODflow;
		fprintf(StatFile, "%g \t%d \t%g \n", (double)MaxFlow, CumODpairs, (double)CumODflow); 
	}
	fprintf(StatFile, "\n\n");

}



