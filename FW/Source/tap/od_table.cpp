#include "stdafx.h"

#include <stdio.h>    /* FILE */
#include <stdlib.h>   /* calloc, free */
#include <math.h>     /* ldexp, frexp, ceil */

#include "os.h"
#include "my_types.h"
#include "my_util.h"
#include "message.h"
#include "meta.h"
#include "od_table.h"


void Clear_ODtable (my_float **ODtable, int no_zones) {
	int Orig, Dest;

	for(Orig=1;Orig<=no_zones;Orig++)
		for(Dest=1;Dest<=no_zones;Dest++)
			ODtable[Orig][Dest]=0.0;
}

void Factor_ODtable (my_float **ODtable, my_float Factor, int no_zones) {
	int Orig, Dest;

	for(Orig=1;Orig<=no_zones;Orig++)
		for(Dest=1;Dest<=no_zones;Dest++)
			ODtable[Orig][Dest]*=Factor;
}

my_float Sum_ODtable (my_float **ODtable, int no_zones){
	int Orig, Dest;
	my_float sum=0.0;

	for(Orig=1;Orig<=no_zones;Orig++)
		for(Dest=1;Dest<=no_zones;Dest++)
			sum+=ODtable[Orig][Dest];
	return(sum);
}


my_float SumAbs_ODtable (my_float **ODtable, int no_zones){
	int Orig, Dest;
	my_float sum=0.0;

	for(Orig=1;Orig<=no_zones;Orig++)
		for(Dest=1;Dest<=no_zones;Dest++)
			sum+=(my_float) fabs( (double) ODtable[Orig][Dest]);
	return(sum);
}




/****************    INPUT OUTPUT  ********************/


/* Reads an OD table in the TransLab format. 
An open input file is given, from which metadata has been read by the calling function. */

void Read_ODtable (FILE *ODtableFile, char *ODtableFileName, my_float **ODtable, int no_zones) {
	char ch, Coloumn[2], Semicoloumn[2]; /* Reserve room for the '\0' in the fscanf. */
	int Orig, Dest, NewOrig, NewDest;
	double Value;
	
	SkipComments(ODtableFile);

	Orig = Dest = 1;
	while ( Orig<=no_zones && !feof ( ODtableFile ) ) {

		SkipComments(ODtableFile);

		if( fscanf(ODtableFile, " Origin %d", &NewOrig) == 1 ) {
			if ( NewOrig > no_zones || NewOrig < 1 )
				FatalInputError(ODtableFile, ODtableFileName,
						"Origin %d is out of the range (1 to %d) ", NewOrig, no_zones);
			if ( NewOrig < Orig )
				FatalInputError(ODtableFile, ODtableFileName,
						"Origin %d can not come after origin %d ", NewOrig, Orig);
			for(;Orig<NewOrig;Orig++) {
				for(;Dest<=no_zones;Dest++) {
					ODtable[Orig][Dest]=0.0;
				}
				Dest=1;
			}
			Dest=1;
			Orig = NewOrig; /* Just to make sure. */
		}

		else if ( fscanf(ODtableFile, " %d  %1[:] %lf %1[;]", &NewDest, Coloumn, &Value, Semicoloumn) == 4) {
			if ( NewDest > no_zones || NewDest < 1 )
				FatalInputError(ODtableFile, ODtableFileName,
						"For origin %d, destination %d is out of the range (1 to %d) ",
						Orig, NewDest, no_zones);
			if ( NewDest < Dest )
				FatalInputError(ODtableFile, ODtableFileName,
						"For origin %d, destination %d can not come after destination %d ",
						Orig, NewDest, Dest);
			if ( Value < 0.0 )
				FatalInputError(ODtableFile, ODtableFileName,
						"Negative number of trips (%f) from origin %d to destination %d ",
						Value, Orig, NewDest);
			for(;Dest<NewDest;Dest++) {
				ODtable[Orig][Dest]=0.0;
			}
			Dest = NewDest;
			ODtable[Orig][Dest] = (my_float)Value;
			Dest++;
		}

		else if( fscanf(ODtableFile, " %c", &ch ) != EOF ) {
			FatalInputError(ODtableFile, ODtableFileName,
					"Bad input (%c) after origin %d destination %d ", ch, Orig, Dest);
		}
	}


}



/* Reads an OD table from colomns. 
An open input file is given, from which metadata has been read by the calling function. */

void Read_ODtable_colomns (FILE *ODtableFile, char *ODtableFileName, my_float **ODtable, int no_zones) {
	int Orig, Dest;
	double Value;
	
	Clear_ODtable(ODtable, no_zones);

	SkipComments(ODtableFile);

	while ( !feof ( ODtableFile ) ) {

		if( fscanf(ODtableFile, " %d %d %lf", &Orig, &Dest, &Value) != 3 ) 
			FatalInputError(ODtableFile, ODtableFileName,
					"Bad input at origin %d destination %d ", Orig, Dest);

		if ( Orig > no_zones || Orig < 1 )
			FatalInputError(ODtableFile, ODtableFileName,
					"Origin %d is out of the range (1 to %d) ", Orig, no_zones);

		if ( Dest > no_zones || Dest < 1 )
			FatalInputError(ODtableFile, ODtableFileName,
					"Destination %d is out of the range (1 to %d) ", Dest, no_zones);

		ODtable[Orig][Dest] = (my_float)Value;

		SkipComments(ODtableFile);
	}
}



/* Writes an OD table in the TransLab format. 
An open output file is given, to which metadata has been written by the calling function. */

#define PAIRS_PER_LINE 5

void Write_ODtable (my_float **ODtable, FILE *ODtableFile, int no_zones) {
	int Orig, Dest;
	int PrintCount; 
	double Value;

	for(Orig=1;Orig<=no_zones;Orig++) {
		fprintf(ODtableFile,"\n\nOrigin %d \n", Orig);
		PrintCount=0;
		for(Dest=1;Dest<=no_zones;Dest++) {
			Value = (double) ODtable[Orig][Dest];
			if(Value > 0.0) {
				fprintf(ODtableFile, " %d : %.20g ; ",Dest, Value) ;
				if( ++PrintCount == PAIRS_PER_LINE) {
					fprintf(ODtableFile, "\n");
					PrintCount=0;
				}	
			}
		}
	}

	fprintf(ODtableFile, "\n\n");
}

	
/* Writes an OD table in a three colomns format: origin, destination, value.
Zero values are omitted. 
(This format is readable by TP+) */

void Write_ODtable_colomns (my_float **ODtable, char *OutputFileName, int no_zones) {
	FILE *OutputFile;
	int Orig, Dest;
	double Value;

	OutputFile = FileOpen ( OutputFileName , "w" );
	fprintf(OutputFile,"Origin \tDestination \tValue \n");

	for(Orig=1;Orig<=no_zones;Orig++) {
		for(Dest=1;Dest<=no_zones;Dest++) {
			Value = (double) ODtable[Orig][Dest];
			if(Value > 0.0) 
				fprintf(OutputFile,"%d \t%d \t%.20g \n", Orig, Dest, Value);
		}
	}
	fclose(OutputFile);
}






/****************    HISTOGRAMS  ********************/


void PrintODtableExponentDistributionHeader (int MinExponent, int MaxExponent, FILE *StatFile) {
	int Exponent;

	fprintf(StatFile, "Value <= \t0 \t"); 
	for(Exponent=MinExponent;Exponent<=MaxExponent;Exponent++) 
		fprintf(StatFile, "%g \t", ldexp(1.0,Exponent));
	fprintf(StatFile, "INF \t\tMax value \t\n");
}


void PrintODtableExponentDistribution (my_float **ODtable, int no_zones, int MinExponent, int MaxExponent, FILE *StatFile) {
	int Orig, Dest, Exponent, no_cells, *DistODpairs, ZeroODpairs, SubMinODpairs, OverMaxODpairs, CumODpairs;
	my_float Value, MaxValue, Fraction;

/* Initialize variables */
	no_cells = MaxExponent -  MinExponent +1;
	DistODpairs = (int *) calloc(no_cells+1, sizeof(int));
	for(Exponent=0;Exponent<no_cells;Exponent++)
		DistODpairs[Exponent]=0;
	ZeroODpairs = SubMinODpairs = OverMaxODpairs = 0;
	MaxValue=0.0;

/* Compute distributions */
	for(Orig=1;Orig<=no_zones;Orig++) {
		for(Dest=1;Dest<=no_zones;Dest++) {
			Value = ODtable[Orig][Dest];
			if(Value < 0.0)
				ExitMessage("Can not compute OD table exponent distribution with negative "
					"value %g from origin %d to destination %d.", Value, Orig, Dest);
			if(Value==0.0){
				ZeroODpairs++;				
				continue;
			}
			Fraction = frexp (Value, &Exponent); /* Find the exponent of the value. */
			if (Fraction==0.5) Exponent--;	/* Fix the rounding problem. */
			if (Exponent<=MinExponent)
				SubMinODpairs++; 
			else if (Exponent<=MaxExponent)
				DistODpairs[Exponent-MinExponent]++;
			else{
				OverMaxODpairs++; 
				if(Value>MaxValue) MaxValue=Value;
			}
		}
	}
	fprintf(StatFile, "\t%d \t", ZeroODpairs); 
	CumODpairs=ZeroODpairs+SubMinODpairs;
	fprintf(StatFile, "%d \t", CumODpairs); 
	for(Exponent=0;Exponent<no_cells;Exponent++) {
		CumODpairs+=DistODpairs[Exponent];
		fprintf(StatFile, "%d \t", CumODpairs);
	}
	if(OverMaxODpairs>0){
		CumODpairs+=OverMaxODpairs;
		fprintf(StatFile, "%d \t\t%g \t", CumODpairs, MaxValue);
	}

	fprintf(StatFile, "\n");

	free(DistODpairs);
}


void PrintODtableDistributionHeader (my_float Step, int no_cells, FILE *StatFile) {
	int Cell;

	fprintf(StatFile, "Value <= \t"); 
	for(Cell=0;Cell<=no_cells;Cell++) 
		fprintf(StatFile, "%g \t", (double)Cell*Step);
	fprintf(StatFile, "INF \t\tMax value \t\n");
}



void PrintODtableDistribution (my_float **ODtable, int no_zones, my_float Step, int no_cells, FILE *StatFile) {
	int Orig, Dest, Cell, LastCell, *DistODpairs, OverMaxODpairs, CumODpairs;
	my_float Value, MaxValue;

/* Initialize variables */
	DistODpairs = (int *) calloc(no_cells+1, sizeof(int));
	for(Cell=0;Cell<=no_cells;Cell++) {
		DistODpairs[Cell]=0;
	}
	OverMaxODpairs = 0;
	MaxValue = 0.0;

/* Compute distributions */
	for(Orig=1;Orig<=no_zones;Orig++) {
		for(Dest=1;Dest<=no_zones;Dest++) {
			Value = ODtable[Orig][Dest];
			if(Value < 0.0)
				ExitMessage("Can not compute OD table distribution with negative "
					"value %g from origin %d to destination %d.", Value, Orig, Dest);
			Cell = (int) ceil(Value/Step);
			if (Cell>no_cells){
				OverMaxODpairs++; 
				if(Value>MaxValue) MaxValue=Value;
			}
			else {
				DistODpairs[Cell]++;
			}
		}
	}

/* Find the last non-empty cell. */
	for(LastCell=no_cells;DistODpairs[LastCell]==0;LastCell--);

	fprintf(StatFile, "\t");

	CumODpairs=0;
	for(Cell=0;Cell<=LastCell;Cell++) {
		CumODpairs+=DistODpairs[Cell];
		fprintf(StatFile, "%d \t", CumODpairs);
	}
	if(OverMaxODpairs>0){
		CumODpairs+=OverMaxODpairs;
		fprintf(StatFile, "%d \t\t%g \t", CumODpairs, (double)MaxValue); 
	}
	fprintf(StatFile, "\n");


	free(DistODpairs);

}






void ODtableFreqDist_Vert(my_float **Weight, int no_zones, int **Value, int max_value, char* ValueName, char* Title, FILE* FreqDistFile){
	int Orig, Dest, x;
	my_float *Sum;

	Sum = (my_float *) calloc(max_value+1, sizeof(my_float));

	for(x=0;x<=max_value;x++){
		Sum[x] = 0.0;
	}

	for(Orig=1;Orig<=no_zones;Orig++){
		for(Dest=1;Dest<=no_zones;Dest++){
			x = Value[Orig][Dest];
			Sum[x] += Weight[Orig][Dest];
		}	
	}

	fprintf( FreqDistFile, "%s \t%s \t\n", ValueName, Title);
	for(x=0;x<=max_value;x++)
		fprintf( FreqDistFile, "%d \t%.20g \t\n", x, Sum[x]);
	fprintf( FreqDistFile, "\n");

	free(Sum);

}



void ODtableFreqDist(my_float **Weight, int no_zones, int **Value, int max_value, char* Title, FILE* FreqDistFile){
	int Orig, Dest, x;
	my_float *Sum;

	Sum = (my_float *) calloc(max_value+1, sizeof(my_float));

	for(x=0;x<=max_value;x++){
		Sum[x] = 0.0;
	}

	for(Orig=1;Orig<=no_zones;Orig++){
		for(Dest=1;Dest<=no_zones;Dest++){
			x = Value[Orig][Dest];
			Sum[x] += Weight[Orig][Dest];
		}	
	}

	fprintf( FreqDistFile, "%s \t", Title);
	for(x=0;x<=max_value;x++)
		fprintf( FreqDistFile, "%.20g \t", Sum[x]);
	fprintf( FreqDistFile, "\n");

	free(Sum);

}

void PrintFreqDistHeader(FILE *FreqDistFile, int max_value, char* ValueName){
	int x;

	fprintf( FreqDistFile, "%s \t", ValueName);
	for(x=0;x<=max_value;x++)
		fprintf( FreqDistFile, "%d \t", x);
	fprintf( FreqDistFile, "\n");
}


