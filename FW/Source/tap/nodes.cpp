
#include "stdafx.h"

# include <stdio.h>
# include <string.h>
# include <time.h>
# include <math.h>
# include <stdlib.h>
# include <malloc.h>
# include <errno.h>
# include <ctype.h>

#include "os.h"
#include "my_types.h"
#include "md_alloc.h"
#include "my_util.h"
#include "message.h"
#include "tui.h"

/* Function declarations. */
int Find_OD_dist ( int ** OD_dist, int no_zones, char* NodesFileName);
void Read_Nodes (my_float *Xnode, my_float *Ynode, int no_nodes, char* NodesFileName) ;

my_float ft2mile(int x);


/* Main functions of this module. */

int Find_OD_dist ( int ** OD_dist, int no_zones, char* NodesFileName){
	int Orig, Dest, dist, max_orig, max_dest, max_dist=0;
	my_float *Xnode, *Ynode;
	my_float dx,dy;

	Xnode = (my_float *) Alloc_1D(no_zones, sizeof(my_float));
	Ynode = (my_float *) Alloc_1D(no_zones, sizeof(my_float));

	Read_Nodes (Xnode, Ynode, no_zones, NodesFileName);

	for(Orig=1;Orig<=no_zones;Orig++){
		for(Dest=1;Dest<=no_zones;Dest++){
			dx = Xnode[Orig]-Xnode[Dest];
			dy = Ynode[Orig]-Ynode[Dest];
			dist = (int) ceil(sqrt((double) dx*dx+dy*dy));
			OD_dist[Orig][Dest] = dist;
			if(dist>max_dist) {
				max_dist=dist;
				max_orig=Orig;
				max_dest=Dest;
			}
		}
	}

	printf("Maximum OD distance %d from origin %d to destination %d .\n", max_dist, max_orig, max_dest);

	free(Xnode);
	free(Ynode);

	return(max_dist);
}


void Read_Nodes (my_float *Xnode, my_float *Ynode, int no_nodes, char* NodesFileName) {
	FILE *NodesFile;

	char ch;
	int Node=0, NewNode, X, Y;

	if(NodesFileName==NULL || NodesFileName[0]=='-') {
		for(Node=1;Node<=no_nodes;Node++){
			Xnode[Node]=Ynode[Node]=0.0;
		}
		return;
	}

	NodesFile = FileOpen ( NodesFileName , "r" );
	SkipComments(NodesFile);

	fscanf(NodesFile, " node X Y ; ");

	while ( Node<no_nodes && !feof ( NodesFile ) ) {

		SkipComments(NodesFile);

		if( fscanf(NodesFile, " %d %d %d %c ", &NewNode, &X, &Y, &ch) == 4 ) {
			if(NewNode==Node+1){
				Node++;
			}
			else {
				printf("Error in node file %s: Last node %d New node %d \n",
						NodesFileName, Node, NewNode);
				exit(EXIT_FAILURE);
			}
			if(ch!=';'){
				printf("Error in node file %s: Expected semicolon for node %d, found %c \n",
						NodesFileName, Node, ch);
				exit(EXIT_FAILURE);
			}
				
		}
		else {
			printf("Error in node file %s: Wrong number of arguments after node %d \n",
					NodesFileName, Node);
			exit(EXIT_FAILURE);
		}			

		Xnode[Node] = ft2mile(X);
		Ynode[Node] = ft2mile(Y);
	}

	if(Node!=no_nodes){
		printf("Error in node file %s: last node is %d, number of nodes is %d \n",
				NodesFileName, Node, no_nodes);
		exit(EXIT_FAILURE);
	}

	if( fscanf(NodesFile, " %c", &ch ) != EOF )
		InputWarning("Expected EOF in file '%s' after node %d, and not %c ", NodesFileName, Node, ch	);

}




my_float ft2mile(int x){
	return((my_float)x/5280);
}




