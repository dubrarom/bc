#include "stdafx.h"

#include <stdio.h>
#include <stdlib.h>

#include "os.h"
#include "my_types.h"
#include "my_util.h"
#include "message.h"
#include "tui.h"
#include "links.h"
#include "link_functions.h"
#include "fw_status.h"
#include "ls_bisect.h"

extern FILE* LogFile;
extern int no_zones, no_nodes, no_links;
extern my_float **ODflow, TotalODflow;

int	AssignIterations;
double 	RelativeGapTarget;
static double TimeLimit=1000;	/* hours, to be converted to seconds in Init */

static FILE* FW_Table;


int ContinueFW(struct fw_status_struct fw_status){
	if( fw_status.Iteration<=AssignIterations && fw_status.RelativeGap>RelativeGapTarget && my_clock()<TimeLimit )
		return(TRUE);
	else
		return(FALSE);
}


void InitFWstatus(struct fw_status_struct *fw_status){
	char *FW_TableFileName;

	tuiGetInt("Assignment iterations {ASITE}", TRUE, &AssignIterations);
	tuiGetDouble("Relative gap target {RGT}", TRUE, &RelativeGapTarget);
	tuiGetDouble("Time limit (hours) {TLIM}", FALSE, &TimeLimit);
	TimeLimit*=3600;	/* convert to seconds. */


	(*fw_status).RelativeGap = RelativeGapTarget+1.0;
	(*fw_status).BestLowerBound=0-1.0e10;
	(*fw_status).Iteration=0;

	FW_TableFileName=NULL;
	tuiGetOutputFileName( "FW table output file name", TRUE, &FW_TableFileName);

	FW_Table = FileOpen ( FW_TableFileName,"w");
	fprintf( FW_Table , "Type \tNumber \tTime \tObjective function \tAverage excess cost \t"
			"Lower bound \tBest Lower Bound \tGap \tRelative Gap \t");
	print_ls_header(FW_Table);
	fprintf( FW_Table, "\n");
	fprintf( FW_Table , "Start FW \t\t" TIME_FORMAT "\n", my_clock() );
	fflush(FW_Table);

	fprintf(LogFile , "Traffic Assignment Problem solved by Frank Wolfe\n"
			"Trees rooted at origins. \n\n");
	fprintf(LogFile , "Assignment iterations %d  \n", AssignIterations);
	fprintf(LogFile , "Relative Gap target %g \n", RelativeGapTarget);
	fprintf(LogFile, "Time Limit %g \n", TimeLimit);
	fflush(LogFile);

}



void FirstFWstatus(my_float *MainVolume, struct fw_status_struct *fw_status){

	(*fw_status).OF = OF_Links (MainVolume);

	fprintf( FW_Table, "FW Iteration \t%d \t", (*fw_status).Iteration);
	fprintf( FW_Table , TIME_FORMAT, my_clock() );
	fprintf( FW_Table , "%.20g \t\n", (*fw_status).OF );
	fflush(FW_Table);

	StatusMessage("Iteration", "FW iteration : %d", (*fw_status).Iteration);
	StatusMessage("OF", "OF %.20lg", (double) (*fw_status).OF);

}


void UpdateFWstatus(my_float *MainVolume, my_float *SDVolume, struct fw_status_struct *fw_status){

	(*fw_status).OF = OF_Links (MainVolume);
	(*fw_status).Gap = -OF_LinksDirectionalDerivative(MainVolume, SDVolume, 0.0 );
	(*fw_status).LowerBound = (*fw_status).OF -(*fw_status).Gap; 
	if( (*fw_status).LowerBound > (*fw_status).BestLowerBound ) 
		(*fw_status).BestLowerBound = (*fw_status).LowerBound ;
	if ((*fw_status). BestLowerBound != 0.0 ) 
		(*fw_status).RelativeGap = (( (*fw_status).OF - (*fw_status).BestLowerBound ) / FABS((*fw_status).BestLowerBound) ) ;
	(*fw_status).AverageExcessCost = -TotalLinkCost(SDVolume)/TotalODflow;

	fprintf( FW_Table, "FW Iteration \t%d \t", (*fw_status).Iteration);
	fprintf( FW_Table , TIME_FORMAT, my_clock() );
	fprintf( FW_Table , OF_FORMAT, (double) (*fw_status).OF );
	fprintf( FW_Table , FLOAT_FORMAT, (double) (*fw_status).AverageExcessCost );
	fprintf( FW_Table , OF_FORMAT, (double) (*fw_status).LowerBound);
	fprintf( FW_Table , FLOAT_FORMAT, (double) (*fw_status).BestLowerBound );
	fprintf( FW_Table , FLOAT_FORMAT, (double) (*fw_status).Gap );
	fprintf( FW_Table , FLOAT_FORMAT, (double) (*fw_status).RelativeGap );
	ls_report(FW_Table);
	fprintf(FW_Table, "\n");
	fflush(FW_Table);

	ReportInterLinkFlows(MainVolume, (*fw_status).Iteration);

	StatusMessage("Iteration", "FW iteration : %d", (*fw_status).Iteration);
	StatusMessage("OF", "OF %g  Rgap %g  AEC %g", (double) (*fw_status).OF, (double) (*fw_status).RelativeGap, (double) (*fw_status).AverageExcessCost); 

}


void CloseFWstatus(my_float *MainVolume){
	int congested_links;

	ReportFinalLinkFlows(MainVolume);

	fprintf( FW_Table , "End run \t\t" TIME_FORMAT "\n",my_clock());
	fprintf( FW_Table , "Ended: %s \n\n", Date_and_Time() );
	fclose(FW_Table);

	congested_links = CongestedLinks(MainVolume);
	fprintf(LogFile, "\n\nIn %d links out of %d (%f%%) volume exceeds capacity. \n\n",
			congested_links, no_links, (double) 100 * congested_links / no_links);


}


