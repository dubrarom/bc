#include "stdafx.h"

#include <stdio.h> 	/* FILE shows up in other include files */
#include <stdlib.h> 	/* free */

#include "os.h"
#include "my_types.h"
#include "md_alloc.h"
#include "my_util.h"
#include "message.h"
#include "mincostroutes.h"
#include "links.h"
#include "link_functions.h"

extern int no_zones, no_nodes, no_links, FirstThruNode;


/* Find the minimum cost route tree for a single origin*/

#define INVALID -1			/* Represents an invalid value. */
#define WAS_IN_QUEUE -7			/* Shows that the node was in the queue before. (7 is for luck.) */


int Minpath ( int Orig , int *PredLink, my_float *CostTo ){
	int node , now, NewNode, k, Return2Q_Count=0;
	my_float NewCost;
	int *QueueNext;
	int QueueFirst, QueueLast;

	QueueNext = (int *) Alloc_1D(no_nodes, sizeof(int));

	for ( node=1; node<=no_nodes; node++){
		QueueNext[node] = INVALID;
		CostTo[node] = 1.0e+15;
		PredLink[node] = INVALID;
	}

	now = Orig;
	QueueNext[now] = WAS_IN_QUEUE;
	PredLink[now] = INVALID;
	CostTo[now] = 0.0;

	QueueFirst = QueueLast = INVALID;


	while ( (now != INVALID) && (now != WAS_IN_QUEUE) ) {
		if(now>=FirstThruNode || now==Orig) {

			for ( k = FirstLinkFrom[now] ; k<=LastLinkFrom[now] ; k++ ) {
						/* For every link that terminate at "now": */

				NewNode = Link[k].Head;
				NewCost = CostTo[now] + Link[k].GenCost;

				if ( CostTo[NewNode] > NewCost ) {
/* If the new lable is better than the old one, correct it, and make sure that the new node to the queue. */

					CostTo[NewNode] = NewCost;
					PredLink[NewNode] = k;

/* If the new node was in the queue before, add it as the first in the queue. */
					if ( QueueNext[NewNode] == WAS_IN_QUEUE ) {
						QueueNext[NewNode] = QueueFirst;
						QueueFirst = NewNode;
						if ( QueueLast == INVALID )
							QueueLast = NewNode;
						Return2Q_Count++;
					}

/* If the new node is not in the queue, and wasn't there before, add it at the end of the queue. */
					else if ( QueueNext[NewNode] == INVALID && NewNode!=QueueLast) {
						if(QueueLast!=INVALID) { 					/*Usually*/
							QueueNext[QueueLast] = NewNode;
							QueueLast = NewNode;
						}
						else {			  /* If the queue is empty, initialize it. */
							QueueFirst = QueueLast = NewNode;
							QueueNext[QueueLast] = INVALID;
						}
					}

/* If the new node is in the queue, just leave it there. (Do nothing) */
				}
			}
		}

/* Get the first node out of the queue, and use it as the current node. */
		now = QueueFirst;
		if ( (now == INVALID) || (now == WAS_IN_QUEUE) )	break;

		QueueFirst = QueueNext[now];
		QueueNext[now] = WAS_IN_QUEUE;
		if ( QueueLast == now ) QueueLast = INVALID ;
	}

	free(QueueNext);

	return(Return2Q_Count);
}



/* Find minimum cost routes .
Input: 	None
Output:	RouteCost - route generalized cost, by origin and destination
	MinPathSuccLink - trees of minimum cost routes, by destination and node. */

void FindMinCostRoutes (int **MinPathPredLink, my_float **RouteCost) {
	int Orig , Dest ;
	my_float *CostTo;

	CostTo = (my_float *) Alloc_1D(no_nodes, sizeof(my_float));
	StatusMessage("Minpath", "Starting the minpath calculations.");
	for ( Orig=1 ; Orig<=no_zones ; Orig++ ) {
		StatusMessage("Minpath","Searching minpath for origin %6d.", Orig);
		Minpath(Orig, MinPathPredLink[Orig], CostTo );
		if( RouteCost!=NULL){
			for ( Dest=1 ; Dest<=no_zones ; Dest++ ) {
				if( CostTo[Dest] < 0.0 )
					ExitMessage("Negative cost %lg from Origin %d to Destination %d.",
							 (double)CostTo[Dest],Orig,Dest);
				RouteCost[Orig][Dest] = CostTo[Dest];
			}
		}
	}
	free(CostTo);	StatusMessage("Minpath","Found all minpath.");
}


/* Assign OD flows to links according to the routes in MinPathPredLink. */

void Assign(my_float **ODflow, int **MinPathPredLink, my_float *Volume ){
	int Dest, Orig, k ;
	int CurrentNode;
	my_float RouteFlow;

	ClearVolume (Volume);

	StatusMessage("Assign", "Starting assign.");
	for ( Orig=1; Orig<=no_zones; Orig++) {
		StatusMessage("Assign","Assigning origin %6d.", Orig);
		for ( Dest=1; Dest<=no_zones; Dest++) {
			if ( Dest==Orig ) continue;
			RouteFlow = ODflow[Orig][Dest];
			if (RouteFlow==0) continue;
			CurrentNode=Dest;
			while ( CurrentNode != Orig ) {
				k = MinPathPredLink[Orig][CurrentNode];
				if (k==INVALID) {
					Warning("A problem in mincostroutes.c (Assign): Invalid pred for node %d Orig %d \n\n", CurrentNode, Orig);
					break;
				}
				Volume[k] += RouteFlow;
				CurrentNode = Link[k].Tail;
			}
		}
	}
	StatusMessage("Assign", "Finished assign.");
}



/* Assign OD flows to links according to the routes in MinPathPredLink. */

void AssignOrigin(int Orig, my_float *DestFlow, int *PredLink, my_float *Volume ){
	int Dest, k ;
	int CurrentNode;
	my_float RouteFlow;

	ClearVolume (Volume);
	for ( Dest=1; Dest<=no_zones; Dest++) {
		if ( Dest==Orig ) continue;
		RouteFlow = DestFlow[Dest];
		if (RouteFlow==0) continue;
		CurrentNode=Dest;
		while ( CurrentNode != Orig ) {
			k = PredLink[CurrentNode];
			if(Orig==226 && k==1024){
				printf(" ");
			}
			if (k==INVALID) {
				Warning("A problem in mincostroutes.c (Assign): Invalid pred for node %d Orig %d \n\n", CurrentNode, Orig);
				break;
			}
			Volume[k] += RouteFlow;
			CurrentNode = Link[k].Tail;
		}
	}
}


/* Find cost components along the routes given in MinPathPredLink.*/
void RouteCostComponents (int **MinPathPredLink, cost_vector **RouteCosts) {
	int Orig , Dest, k, Comp;
	cost_vector *Costs;
	int CurrentNode;


	StatusMessage("RouteCostComponents", "Starting route cost components computation.");
	for ( Orig=1; Orig<=no_zones; Orig++) {
		StatusMessage("RouteCostComponents", "Computing route cost components for origin %6d.", Orig);
		for ( Dest=1; Dest<=no_zones; Dest++) {
			Costs = RouteCosts[Orig] + Dest; /*&(RouteCosts[Orig][Dest]);*/
			for(Comp=0;Comp<=NO_COSTPARAMETERS;Comp++)
				(*Costs)[Comp]=0.0;
			if ( Dest!=Orig ) {
				CurrentNode=Dest;
				while ( CurrentNode != Orig ) {
					k = MinPathPredLink[Orig][CurrentNode];
					(*Costs)[IVTT] += Link[k].Delay;
					(*Costs)[DIST] += Link[k].Distance;
					(*Costs)[MONETARY] += Link[k].Toll;
					CurrentNode = Link[k].Tail;
				}
			}
		}
	}
	StatusMessage("RouteCostComponents", "Finished route cost components computation.");
}





my_float AverageExcessCost (my_float **ODflow, my_float ODflowTotal, my_float *MainVolume) {
	my_float *SubVolume, *SDVolume;
	int **MinPathPredLink;
	my_float AEC;

	MinPathPredLink = (int**)Alloc_2D(no_zones,no_nodes, sizeof(int));
	SDVolume = SubVolume = (my_float*)Alloc_1D(no_links, sizeof(my_float) );
	UpdateLinkCost(MainVolume);
	FindMinCostRoutes (MinPathPredLink, NULL);
	Assign (ODflow,MinPathPredLink,SubVolume);
	VolumeDifference( SubVolume, MainVolume, SDVolume);	/* Which yields the search direction. */
	AEC = -TotalLinkCost(SDVolume)/ODflowTotal;
	Free_2D((void**)MinPathPredLink,no_zones,no_nodes);
	free(SubVolume);
	
	return (AEC);
}

