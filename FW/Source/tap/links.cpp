#include "stdafx.h"

# include <stdio.h> 	/* FILE */
# include <stdlib.h> 	/* free */
# include <math.h> 	/* pow */

#include "os.h"
#include "my_types.h"
#include "md_alloc.h"
#include "my_util.h"
#include "message.h"
#include "tui.h"
#include "meta.h"
#include "links.h"
#include "link_functions.h"


int no_zones, no_nodes, no_links, FirstThruNode;
struct link_record *Link;
int *FirstLinkFrom;
int *LastLinkFrom;
sorted_list* LinksTo;

double AutoCostCoef[NO_COSTPARAMETERS];
double OFscale=1.0;
struct meta_struct LinksMetaData;
static FILE *InterFlowFile;
static int StoreLinksPeriod=1;	/* By default if there is a file for name for storing link flows, store at all iterations. */

/* Internal functions */

my_float Link_Delay (  int k , my_float *Volume ){
	return (Link[k].FreeTravelTime * ( 1.0 + Link[k].BoverC * ( pow ( Volume[k], Link[k].Power ) ) ) );
}

my_float LinkDelay_Integral (  int k , my_float *Volume ){
	if(Link[k].Power>=0.0)
		return (Volume[k] * Link[k].FreeTravelTime * ( 1.0 + (Link[k].BoverC/(Link[k].Power+1)) * pow(Volume[k], Link[k].Power) ));
	else 
		return 0.0;
}

my_float Link_Delay_Der (  int k, my_float *Volume ){
	if(Link[k].Power==0.0)
		return 0.0;
	else
		return (Link[k].FreeTravelTime * Link[k].BoverC * Link[k].Power * pow(Volume[k],(Link[k].Power-1))) ;
}


my_float AdditionalCost ( int k) {
	my_float AddCost;

	AddCost = 	AutoCostCoef[DIST] * Link[k].Distance +
			+AutoCostCoef[MONETARY] * Link[k].Toll;

	return  AddCost;
}




my_float Link_GenCost(int k, my_float *Volume){
	return (Link[k].AdditionalCost + Link_Delay(k, Volume));
}

my_float LinkCost_Integral( int k, my_float *Volume){

	return (Link[k].AdditionalCost*Volume[k] + LinkDelay_Integral(k, Volume));
}

my_float Link_GenCostDer( int k, my_float *Volume){

	return (Link_Delay_Der( k, Volume));
}






/* External functions */

void ClearVolume( my_float *VolumeArray){
	int k;
	for ( k=1; k<=no_links; k++)
		VolumeArray[k] = 0.0;
}

void VolumeDifference( my_float *Volume1, my_float *Volume2, my_float *Difference){
	int k;
	for ( k=1; k<=no_links; k++)
		Difference[k] = Volume1[k] - Volume2[k];
}
	

void UpdateVolume( my_float *MainVolume, my_float *SDVolume, my_float Lambda){
	int k;
	for ( k=1; k<=no_links; k++) {
		 MainVolume[k] += Lambda * SDVolume[k];
	}
}

void UpdateLinkAdditionalCost (void)	{
	int k;

	for ( k=1; k<=no_links; k++ )
		Link[k].AdditionalCost = AdditionalCost(k);
}

void UpdateLinkCost (my_float *MainVolume)	{
	int k;

	for ( k=1; k<=no_links; k++ ){
		Link[k].Delay = Link_Delay(k,MainVolume);
		Link[k].GenCost = Link_GenCost(k,MainVolume);
	}
}

void UpdateLinkCostDer (my_float *MainVolume)	{
	int k;

	for ( k=1; k<=no_links; k++ ){
		Link[k].GenCostDer = Link_GenCostDer(k,MainVolume);
	}
}


void GetLinkTravelTimes(my_float *Volume, my_float *TravelTime){
	int k;

	for ( k=1; k<=no_links; k++ ){
		TravelTime[k]=Link_Delay (k, Volume);
	}
}

my_float TotalLinkCost(my_float *Volume) {
	int k;
	my_float Sum=0;

	for( k=1; k<=no_links ; k++ )
		Sum += Link[k].GenCost * Volume[k];
	return Sum;
}






my_float OF_Links ( my_float *MainVolume){
	int k;
	my_float Sum=0;

	for( k=1; k<=no_links ; k++ )
		Sum+= LinkCost_Integral ( k, MainVolume);

	return Sum / OFscale;
}



my_float OF_LinksDirectionalDerivative( my_float *MainVolume,	my_float *SDVolume, my_float Lambda )	{
	int k;
	my_float *Volume;
	my_float LinkCostSum = 0;

	Volume= (my_float*)Alloc_1D(no_links, sizeof(my_float) );

	for ( k=1; k<=no_links; k++ ) {
		Volume[k] = MainVolume[k] + Lambda * SDVolume[k];
	}
	for ( k=1; k<=no_links; k++ ) {
		LinkCostSum += Link_GenCost(k, Volume) * SDVolume[k];
	}

	free(Volume);
	return LinkCostSum / OFscale;
}







void GetLinkParameters(my_float *FreeTravelTime, my_float *Capacity, my_float *Power, my_float *B_factor){
	int k;

	for ( k=1; k<=no_links; k++ ){
		FreeTravelTime[k]=Link[k].FreeTravelTime;
		Capacity[k]=Link[k].Capacity;
		Power[k]=Link[k].Power;
		B_factor[k]=Link[k].B;	
	}
}

void SetLinkParameters(my_float *FreeTravelTime, my_float *Capacity, my_float Power, my_float B_factor){
	int k;

	for ( k=1; k<=no_links; k++ ){
		Link[k].FreeTravelTime=FreeTravelTime[k];
		Link[k].Capacity=Capacity[k];
		Link[k].Power=Power;
		Link[k].B=B_factor;	
	}
}






/* Initialization functions */


static void ReadLinks (FILE *LinksFile, char *LinksFileName) {
	char ch, Semicolon[2]; /* Reserve room for the '\0' that is written by the scanf. */
	int count, k, Tail, Head, Type;
	double Capacity, Distance, FreeTravelTime, B, Power, Speed, Toll;
	char *LinkArgumentDescription[10]={"tail","head","capacity","length",
		"free flow travel time","power","speed limit","toll","link type","semicolomn - for end of link"};
		/* Descriptions of the various link record arguments, used for error messages. */


	for ( k=1; k<=no_links; k++ ) {
		SkipComments(LinksFile);
		count = fscanf(LinksFile, "%d %d %lf %lf %lf %lf %lf %lf %lf %d %1[;]",
							&Tail, &Head, &Capacity, &Distance, &FreeTravelTime,
							&B, &Power, &Speed, &Toll, &Type, Semicolon);
		if ( count == -1 )
			FatalInputError(LinksFile, LinksFileName, "Can not read link %d. (Probably EOF)", k);
		if ( count != 11 )
			FatalInputError(LinksFile, LinksFileName,
					"Can not read argument number %i (%s) of link %d ",
						count+1, LinkArgumentDescription[count], k);
		if ( Tail > no_nodes || Tail<=0)
			FatalInputError(LinksFile, LinksFileName,
					"Link %d tail-node %d is out of range (1 - %d) ",
					k, Tail, no_nodes );
		if ( Head > no_nodes || Head<=0)
			FatalInputError(LinksFile, LinksFileName,
					"Link %d head-node %d is out of range (1 - %d) ",
					k, Head, no_nodes );
		
		Link[k].Tail=Tail; 
		Link[k].Head=Head; 
		Link[k].Capacity=(my_float)Capacity; 
		Link[k].Distance=(my_float)Distance;
		Link[k].FreeTravelTime=(my_float)FreeTravelTime;
		Link[k].B=(my_float)B;
		Link[k].Power=(my_float)Power;
		Link[k].Speed=(my_float)Speed; 
		Link[k].Toll=(my_float)Toll;
		Link[k].Type=Type;

		if(Power<0)
			InputWarning("Link file '%s', link %d power %lf < 0 is decreasing cost",
					LinksFileName, k, Link[k].Power);

		if(Capacity>0)
			Link[k].BoverC=B/pow(Capacity,Power);
		else {
			Link[k].BoverC=0;
			InputWarning("link file '%s', link %d from %d to %d has %lf capacity \n",
					LinksFileName, k, Tail, Head, Capacity);
		}
	}

	if( fscanf(LinksFile, " %c", &ch ) != EOF )
		FatalInputError(LinksFile, LinksFileName,
				"Expected EOF after link %d from %d to %d, and not %c ", k, Tail, Head, ch);

	fclose(LinksFile);
}



static void InitLinkPointers(char *LinksFileName){
	int k, Node, Tail;

	FirstLinkFrom = (int *) Alloc_1D( no_nodes, sizeof(int));
	LastLinkFrom = (int *) Alloc_1D( no_nodes, sizeof(int));

	FirstLinkFrom[1] = 1;
	Node=1;

	for( k=1; k<=no_links; k++) {
		Tail = Link[k].Tail;
		if(Tail==Node)
			continue;
		else if ( Tail == Node+1 ) {
			LastLinkFrom[Node] = k-1;
			Node=Tail;
			FirstLinkFrom[Node] = k;
		}

/**********************
CHECKING FOR SORT ERRORS AND GAPS IN THE LINKS FILE.
*********************/
		else if( Tail < Node ) 
			ExitMessage("Sort error in link file '%s': a link from node %d was found after "
					"a link from node %d \n", LinksFileName, Tail, Node);
		else if( Tail > Node+1 ) {
			InputWarning("link file '%s' has no links out from "
					"nodes %d through %d. \n", LinksFileName, Node+1, Tail-1);
			LastLinkFrom[Node] = k-1;
			for(Node++;Node<Tail;Node++) {
				FirstLinkFrom[Node] = 0;
				LastLinkFrom[Node] = -1;
			}
			FirstLinkFrom[Node] = k; /* Node equals Tail now. */
		}
	}

	if(Node==no_nodes) {
		LastLinkFrom[Node] = no_links;	/* Now Node equals no_nodes in any case */
	}
	else {
		InputWarning("link file '%s' has no links out from "
				"nodes %d through %d. \n", LinksFileName, Node+1, no_nodes);
		LastLinkFrom[Node] = k-1;
		for(Node++;Node<=no_nodes;Node++) {
			FirstLinkFrom[Node] = 0;
			LastLinkFrom[Node] = -1;
		}
	}

}


void FindLinksTo(void){
	int Node, k;

	LinksTo = (sorted_list*) Alloc_1D(no_nodes, sizeof(sorted_list*));
	for(Node=1;Node<=no_nodes;Node++)
		LinksTo[Node]=NULL;
	for(k=1;k<=no_links;k++)
		ListAdd(k, &(LinksTo[Link[k].Head]));
}
void InitLinks(void){
	char *LinksFileName, *InterFlowFileName;
	FILE *LinksFile;
	int k;

	tuiGetDouble( "Auto cost coeficient for in vehicle travel time [gcu/min] {ACCIVTT}", TRUE, &(AutoCostCoef[IVTT]));
	tuiGetDouble( "Auto cost coeficient for out of vehicle travel time [gcu/min] {ACCOVTT}", TRUE, &(AutoCostCoef[OVTT]));
	tuiGetDouble( "Auto cost coeficient for monetary cost [gcu/cent] {ACCMON}", TRUE, &(AutoCostCoef[MONETARY]));
	tuiGetDouble( "Auto cost coeficient for distance [gcu/mile]  {ACCDIST}", TRUE, &(AutoCostCoef[DIST]));
	tuiGetDouble( "Report scaled objective function divided by", FALSE, &OFscale);

	tuiGetInputFileName( "Link file name", TRUE, &LinksFileName);
	StatusMessage("General", "Reading link data file '%s'", LinksFileName);
	LinksFile = FileOpen ( LinksFileName , "r" );
	LinksMetaData.FileName=LinksFileName;
	ReadMeta(LinksFile, &LinksMetaData) ;
	GetMetaInt("NUMBER OF ZONES", TRUE, &no_zones, LinksMetaData);
	GetMetaInt("NUMBER OF NODES", TRUE, &no_nodes, LinksMetaData);
	GetMetaInt("NUMBER OF LINKS", TRUE, &no_links, LinksMetaData);
	GetMetaInt("FIRST THRU NODE", TRUE, &FirstThruNode, LinksMetaData);
	Link = (struct link_record*) Alloc_1D( no_links, sizeof(struct link_record));
	ReadLinks(LinksFile, LinksFileName); 
	fclose(LinksFile);

	FindLinksTo();

	check7(no_links);

	InitLinkPointers(LinksFileName);
	UpdateLinkAdditionalCost();

	InterFlowFileName=NULL;
	tuiGetOutputFileName( "Intermediate link flows output file name", FALSE, &InterFlowFileName);
	tuiGetInt("Store links if iteration is a product of", FALSE, &StoreLinksPeriod);
	if(InterFlowFileName!=NULL && InterFlowFileName[0]!='-') {
		InterFlowFile=FileOpen(InterFlowFileName,"w");
		fprintf(InterFlowFile, "Tail \t");
		for(k=1;k<=no_links;k++)
			fprintf(InterFlowFile, "%d \t",  Link[k].Tail);
		fprintf(InterFlowFile, "\n");
		fprintf(InterFlowFile, "Head \t");
		for(k=1;k<=no_links;k++)
			fprintf(InterFlowFile, "%d \t",  Link[k].Head);
		fprintf(InterFlowFile, "\n");
		fflush(InterFlowFile);
	}
	else
		InterFlowFile=NULL;

}

void CloseLinks(void){
	int Node;

	FreeMeta(LinksMetaData);
	free(LinksMetaData.FileName);
	free(Link);
	free(FirstLinkFrom);
	free(LastLinkFrom);
	for(Node=1;Node<=no_nodes;Node++)
		ListFree(LinksTo[Node]);
	free(LinksTo);

	if(InterFlowFile!=NULL) 
		fclose(InterFlowFile);

}






/* Report functions */


int CongestedLinks(my_float* Volume){
	int k, congested_links=0;

	for(k=1;k<=no_links;k++) {
		if(Volume[k]> Link[k].Capacity ){
			congested_links++;
		}
	}
	return(congested_links);
}


void ReportInterLinkFlows(my_float *Volume, int Iteration){
	int k;

	if(InterFlowFile!=NULL && Iteration % StoreLinksPeriod == 0  ){
		fprintf(InterFlowFile, "Iteration %d\t",  Iteration);
		for(k=1;k<=no_links;k++)
			fprintf(InterFlowFile, "%g \t",  Volume[k]);
		fprintf(InterFlowFile, "\n");
		fflush(InterFlowFile);
	}
}


void ReportFinalLinkFlows(my_float *Volume){
	FILE *FlowFile;
	char *FlowFileName=NULL;
	int k;

	tuiGetOutputFileName( "Link flows output file name", FALSE, &FlowFileName);
	if(FlowFileName!=NULL && FlowFileName[0]!='-'){
		FlowFile = FileOpen( FlowFileName, "w");

		fprintf(FlowFile, "<NUMBER OF NODES> \t%d \n", no_nodes);
		fprintf(FlowFile, "<NUMBER OF LINKS> \t%d \n", no_links);
		fprintf(FlowFile, "<END OF METADATA> \t \n\n\n");

		fprintf(FlowFile, "~ \tTail \tHead \t: \tVolume \tCost \t; \n");
		for(k=1;k<=no_links;k++) {
			fprintf(FlowFile,"\t%d \t%d \t: \t%.20g \t%.20g \t; \n", Link[k].Tail, Link[k].Head, (double) Volume[k], (double) Link[k].GenCost );
		}
		fclose(FlowFile);
	}
}






void LoadLinkFlowsCosts(char *LoadLinkFlowsFileName, my_float *Volume, int SetCosts){
	FILE *LoadLinkFlowsFile;
	char ch, Colomn[2], Semicolomn[2];
	int k, Tail, Head, count, input_no_nodes, input_no_links;
	double Vol, Cost;
	struct meta_struct LoadLinkFlowsMetaData;

	if(Volume==NULL && !SetCosts)
		return;

	LoadLinkFlowsFile = FileOpen ( LoadLinkFlowsFileName , "r" );
	LoadLinkFlowsMetaData.FileName=LoadLinkFlowsFileName;
	ReadMeta(LoadLinkFlowsFile, &LoadLinkFlowsMetaData) ;
	GetMetaInt("NUMBER OF NODES", TRUE, &input_no_nodes, LoadLinkFlowsMetaData);
	if(input_no_nodes != no_nodes)
		ExitMessage("Load link flows file '%s' is for %d nodes, and not for %d nodes.", LoadLinkFlowsFileName , input_no_nodes, no_nodes);
	GetMetaInt("NUMBER OF LINKS", TRUE, &input_no_links, LoadLinkFlowsMetaData);
	if(input_no_links != no_links)
		ExitMessage("Load link flows file '%s' is for %d links, and not for %d links.", LoadLinkFlowsFileName , input_no_links, no_links);

	for( k = 1; k<=no_links; k++) {
		SkipComments(LoadLinkFlowsFile);
		count = fscanf(LoadLinkFlowsFile, "%d %d %1[:] %lf %lf %1[;]", 
						&Tail, &Head, Colomn, &Vol, &Cost, Semicolomn);

		switch(count){
		case 6: break; /* This is the expected value. */ 
		case 0: FatalInputError(LoadLinkFlowsFile, LoadLinkFlowsFileName, "Can not read tail node value for link %d [%d-%d]", k, Link[k].Tail, Link[k].Head);
			break;
		case 1: FatalInputError(LoadLinkFlowsFile, LoadLinkFlowsFileName, "Can not read head node for link %d [%d-%d]", k, Link[k].Tail, Link[k].Head);
			break;
		case 2: FatalInputError(LoadLinkFlowsFile, LoadLinkFlowsFileName, "Missing colomn for link %d [%d-%d]", k, Link[k].Tail, Link[k].Head);
			break;
		case 3: FatalInputError(LoadLinkFlowsFile, LoadLinkFlowsFileName, "Can not read link flow for link %d [%d-%d]", k, Link[k].Tail, Link[k].Head);
			break;
		case 4: FatalInputError(LoadLinkFlowsFile, LoadLinkFlowsFileName, "Can not read link cost for link %d [%d-%d]", k, Link[k].Tail, Link[k].Head);
			break;
		case 5: FatalInputError(LoadLinkFlowsFile, LoadLinkFlowsFileName, "Missing semicolomn for link %d [%d-%d]", k, Link[k].Tail, Link[k].Head);
			break;
		default: FatalInputError(LoadLinkFlowsFile, LoadLinkFlowsFileName, "Too many data items for link %d [%d-%d]", k, Link[k].Tail, Link[k].Head);
		}

		if ( (Link[k].Tail != Tail) ||  (Link[k].Head != Head) ) 
				FatalInputError(LoadLinkFlowsFile, LoadLinkFlowsFileName, "Expected information for link %d [%d-%d] and not for [%d-%d]. ", 
				k, Link[k].Tail, Link[k].Head, Tail, Head );
		if ( Vol < 0.0 ) 
			FatalInputError(LoadLinkFlowsFile, LoadLinkFlowsFileName, "Negative link flow (%f) for link %d [%d-%d]", Vol, k, Link[k].Tail, Link[k].Head);
		if ( Cost < 0.0 ) 
			FatalInputError(LoadLinkFlowsFile, LoadLinkFlowsFileName, "Negative link cost (%f) for link %d [%d-%d]", Cost, k, Link[k].Tail, Link[k].Head);

		if(Volume!=NULL)
			Volume[k]=Vol;

		if(SetCosts)
			Link[k].GenCost=Cost;

	}

	if( fscanf(LoadLinkFlowsFile, " %c", &ch ) != EOF )
		FatalInputError(LoadLinkFlowsFile, LoadLinkFlowsFileName,	"Expected EOF after link %d [%d-%d], and not %c ", k, Link[k].Tail, Link[k].Head, ch);

	fclose(LoadLinkFlowsFile);

}

