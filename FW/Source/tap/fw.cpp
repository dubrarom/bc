#include "stdafx.h"

#include <stdio.h>
#include <stdlib.h>

#include "os.h"
#include "my_types.h"
#include "md_alloc.h"
#include "my_util.h"
#include "message.h"
#include "tui.h"
#include "meta.h"
#include "link_functions.h"
#include "od_table.h"
#include "od_flow.h"
#include "mincostroutes.h"
#include "ls_bisect.h"
#include "fw_status.h"

extern int no_zones, no_nodes, no_links;

/* Gloabal variables */

my_float **ODflow, TotalODflow;

/* Local Declarations */
/* void FW(void); Should there be a function for fw, or should it be included in main? */
static void Init(char *tuiFileName);
static void Close(char *tuiFileName);
static void InitODflow(void);
static void CloseODflow(void);
/* End of local declarations. */

void main(int argc, char **argv ) {
	my_float *MainVolume, *SubVolume, *SDVolume, Lambda;
	int **MinPathPredLink;
	struct fw_status_struct fw_status;
	char *tuiFileName;

	StatusMessage("General", "Ready, set, go...");
	switch(argc){
		case 2: 	tuiFileName=argv[1];
					break;
		case 1:		tuiFileName="control.tui";
					break;
		default:	ExitMessage("Wrong number of command line arguments (%d). \n"
					"Syntax: fw <text user interface file>.", argc-1);
	}
	Init(tuiFileName);

	MainVolume = (my_float*)Alloc_1D(no_links, sizeof(my_float) );
	SDVolume = SubVolume = (my_float*)Alloc_1D(no_links, sizeof(my_float) ); /* Compute search direction and sub-volume in the same place. */
	MinPathPredLink = (int**)Alloc_2D(no_zones,no_nodes, sizeof(int));

	InitFWstatus(&fw_status);

	FindMinCostRoutes (MinPathPredLink, NULL);
	Assign (ODflow,MinPathPredLink,MainVolume);
	FirstFWstatus(MainVolume, &fw_status);
	UpdateLinkCost(MainVolume);


	for ( fw_status.Iteration = 1; ContinueFW(fw_status); fw_status.Iteration++) {
		FindMinCostRoutes (MinPathPredLink, NULL);
		Assign (ODflow,MinPathPredLink,SubVolume);
		VolumeDifference( SubVolume, MainVolume, SDVolume);	/* Which yields the search direction. */
		Lambda = LinksSDLineSearch ( MainVolume, SDVolume );
		UpdateFWstatus(MainVolume, SDVolume, &fw_status);
		UpdateVolume ( MainVolume, SDVolume, Lambda );
		UpdateLinkCost (MainVolume);
	}

	CloseFWstatus(MainVolume);


	free(MainVolume);
	free(SubVolume);
	Free_2D((void**)MinPathPredLink,no_zones,no_nodes);

	Close(tuiFileName);
	StatusMessage("General","The end");
}



static void Init(char *tuiFileName){
	tuiInit(tuiFileName);
	InitLinks();
	InitODflow();
	InitLineSearch();
}

static void Close(char *tuiFileName){
	StatusMessage("General", "Closing all modules");
	tuiClose(tuiFileName);
	CloseLinks();
	CloseODflow();
	CloseLineSearch();
}

static void InitODflow(void){
	char *ODflowFileName;
	int input_no_zones;
	double Factor=1.0;
	struct meta_struct meta_data;

	tuiGetInputFileName( "OD flow file name", TRUE, &ODflowFileName);
	StatusMessage("General", "Reading OD flow file '%s'", ODflowFileName);
	ODflow = Read_ODflow (ODflowFileName, &TotalODflow, &input_no_zones, &meta_data);
	if(input_no_zones != no_zones)
		ExitMessage("OD flow file '%s' is for %d zones, and not for %d zones.", ODflowFileName , input_no_zones, no_zones);

	tuiGetDouble( "OD flow factor", FALSE, &Factor);
	if(Factor <= 0.0)
		ExitMessage( "OD flow factor %lf is invalid, if must be positive.", Factor);
	if(Factor != 1.0)
		Factor_ODtable (ODflow, (my_float) Factor, no_zones) ;
}


static void CloseODflow(void){
	Free_2D((void **)ODflow, no_zones, no_zones);
}


