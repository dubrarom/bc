#include "stdafx.h"

#include <stdio.h>

#include "my_types.h"
#include "my_util.h"
#include "tui.h"
#include "message.h"
#include "link_functions.h"
#include "ls_bisect.h"


#define MAX_NO_BISECTITERATION 1000  /* Avoids inifinite loops in the second part */


static int MinLineSearchIterations=1;
static int ActualIterations=0;
static my_float LastLambda=1.0;


void InitLineSearch(void){
	tuiGetInt("Line search iterations {LSITE}", FALSE, &MinLineSearchIterations);
}

void CloseLineSearch(void){}


my_float LinksSDLineSearch (my_float *MainVolume, my_float *SDVolume) {
	int n ;
	my_float lambdaleft , lambdaright , lambda ;
	my_float grad;


	grad = OF_LinksDirectionalDerivative ( MainVolume, SDVolume , 0.0 );
	StatusMessage("Line search step", "0.0");
	StatusMessage("Line search grad", "%lf", grad);
	if(grad >= 0) {
		LastLambda=0.0;
		return(0.0);
	}

	grad = OF_LinksDirectionalDerivative ( MainVolume, SDVolume , 1.0 ) ;
	StatusMessage("Line search step", "1.0");
	StatusMessage("Line search grad", "%lf", grad);
	if(grad <= 0) {
		LastLambda=1.0;
		return(1.0);
	}

	lambdaleft=0;
	lambdaright=1;
	lambda=0.5;

	for ( n=1 ; n<=MinLineSearchIterations  ; n++ ) {
		grad = OF_LinksDirectionalDerivative ( MainVolume, SDVolume , lambda );
		StatusMessage("Line search step", "%lf", lambda);
		StatusMessage("Line search grad", "%lf", grad);

		if ( grad <= 0.0 )
			lambdaleft  = lambda;
		else
			lambdaright = lambda;

		lambda = 0.5 * ( lambdaleft + lambdaright );
	}
	for ( ; ((lambdaleft==0) && (n <= MAX_NO_BISECTITERATION))   ; n ++ )	{
		grad = OF_LinksDirectionalDerivative ( MainVolume, SDVolume , lambda );
		StatusMessage("Line search step", "%lf", lambda);
		StatusMessage("Line search grad", "%lf", grad);

		if ( grad <= 0.0 )
			lambdaleft  = lambda;
		else
			lambdaright = lambda;

		lambda = 0.5 * ( lambdaleft + lambdaright );
	}
	ActualIterations = n-1;
	LastLambda = lambdaleft;
	return lambdaleft;
}


void print_ls_header(FILE *fp){
	fprintf(fp, "LS iterations \tStep size \t");
}

void ls_report(FILE *fp){
	fprintf(fp, "%d \t%g \t", ActualIterations, LastLambda);
}

