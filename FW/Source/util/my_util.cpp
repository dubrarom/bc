
#include "stdafx.h"

/*
File name: util.c
Description: Vaiuos usefull utilities, not particularly for this project.
Some are simple extensions of library functions, that include error checking
and handling.

Functions:
Date_and_Time - Returns a string with the current date and time.
my_clock - CPU time in seconds. Prevents the wrap in UNIX.
FileOpen - open a file, handle errors.
make_directory - create a new directory, if it does not exist. Handle errors.
my_gets - get a string of given length from a file.
SkipComments - skip comments (lines starting with ~) in an input file.
ReadValue - read a single value from input file, according to the specified format. Handle errors.
int_switch - swap integers
swap - Swaps two elements of an array.
quick_sort - Sorts a list from element no. left thru element no. right recursively in ascending order.
RandInt - randomly truncate up or down to an integer, probability depends on the fraction.
*/

#include <stdio.h>	/* FILE */
#include <time.h>	/* time, ctime */


#include "os.h"
#include "my_util.h"
#include "message.h"


#ifdef PC
#include <direct.h>	/* mkdir, chdir */
#endif
#ifdef UNIX
#include <sys/types.h> /* mkdir */
#include <sys/stat.h>  /* mkdir */
#include <unistd.h>    /* chdir */
#endif




#define DATE_AND_TIME_BUFFER_LENGTH 50
#define DATE_AND_TIME_END_OF_LINE   25
/* Returns a string with the current date and time. */
char *Date_and_Time(void){
	time_t t;
	struct tm *tt;
	static char buffer[DATE_AND_TIME_BUFFER_LENGTH];

	t = time(NULL); /*time((time_t)0);*/
	tt = gmtime(&t);
	sprintf(buffer, "%s", ctime(&t));

	/* Eliminate the end of line mark at the end of the string. */
	if(buffer[DATE_AND_TIME_END_OF_LINE-1] == '\n')
		buffer[DATE_AND_TIME_END_OF_LINE-1]='\0';
	else
		ExitMessage("Date and time format in ctime has changed, "
			"character %d is no longer the end of the line. Check \n'%s'", DATE_AND_TIME_END_OF_LINE, buffer);
	return(buffer);
}



/* Returns the CPU time (user+system) in seconds, unwrapped. */

#define CLOCK_WRAP 4294.967296 	/* in seconds, equivalent to about 36 minutes */

double my_clock(void){
	static clock_t last_clock=0;
	clock_t new_clock;
	static double wrap=0; /* Stores clock wrap */
		
	new_clock = clock();
	if(new_clock<0 && last_clock>0) wrap+=CLOCK_WRAP;
	if( (new_clock - last_clock) / CLOCKS_PER_SEC  > CLOCK_WRAP / 10 ) 
		printf("Warning: gap between calls to my_clock (util.c) is %f seconds, "
			"the clock wraps every %f seconds, if gap is greater than the "
			"wrap time reported time may be wrong. Make more frequent calls to my_clock.", 
			(double) (new_clock - last_clock) / CLOCKS_PER_SEC, CLOCK_WRAP );
	last_clock = new_clock;

	return( wrap + ((double) new_clock / CLOCKS_PER_SEC) );
}




/* Open a file with FileName and Mode, on error report and exit. */
FILE* FileOpen  (const char *Filename , char *Mode) {
	FILE* fp;
	fp = fopen ( Filename , Mode );
	if (fp==NULL) {
		ExitMessage("Can not open file %s in mode %s.", Filename, Mode);
		return(NULL); /* code is never reached after exit, only to avoid compiler warning. */
	}
	else
		return fp;
}




/* Create a directory by the name DirectoryName, if it does not exist.
On error report and exit. */
void make_directory( char *DirectoryName ) {

	if ( chdir( DirectoryName )==0 )
		return;

	if( mkdir(DirectoryName
#ifdef UNIX 
				,(mode_t)00755 
#endif
						)==0 )
		return;

	ExitMessage("Can not create directory %s ", DirectoryName);
}



/* Get "size-1" characters from file to Buffer. Set the last character to
end of string. The function assumes that Buffer points to an allocated array
of at least "size" characters.*/
void my_gets(FILE* file, char* Buffer, int size) {
	int i, ch;

	for(i=0;i<size-1;i++) {
		ch = fgetc(file);
		if( ch == EOF ) {
			break;
		}
		Buffer[i] = (char) ch;
	}
	Buffer[i]='\0';
}



/* Skip all following comment lines in a file. */
void  SkipComments( FILE *file) {
	char Buffer[10]; /* A little extra memory. */

	while( fscanf(file, " %1[~]", Buffer) == 1 )
		fscanf( file, "%*[^\n]\n");
}



/* Read a single value from file using format and store it in Variable.
On error, report and exit. Path provides the full file name for the error message. */
void	ReadValue( FILE* file, char *format, void *Variable, const char* Path ){
	int count;

	SkipComments(file);
	count = fscanf(file, format, Variable) ;
	if ( count != 1 )
		FatalInputError(file, Path, "Expected: '%s' ", format);
}



/* Swap two elements of an array. */
void swap(int A[], int i, int j){
	int temp;

	temp = A[i];
	A[i] = A[j];
	A[j] = temp;
}


/* Sort list from element no. left thru element no. right recursively
in ascending order. */
void quick_sort(int list[], int left, int right){
	int pivot, i, j;

	if(left < right){
		i = left;
		j = right + 1;
		pivot =  list[left];

		do {
			do {
				i++;
			} while(list[i] < pivot  && i <= right);

			do {
				j--;
			} while(list[j] > pivot && j >= left);

			if(i < j)
				swap(list, i, j);

		} while(i < j);

		swap(list, left, j);
		quick_sort(list, left, j-1);
		quick_sort(list, j+1, right);
	}
}



