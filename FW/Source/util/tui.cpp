
#include "stdafx.h"


#define RELEASEN

#include <stdio.h>	/* FILE */
#include <stdlib.h>	/* malloc, calloc, free */
#include <string.h> 	/* strcpy, strncmp, strlen */
#include <ctype.h>  	/* strtod */
#include <time.h>		/* time, gmtime */

#include "my_util.h"
#include "message.h"
#include "tui.h"

FILE *LogFile;

/* Module Declarations */
static char **tuiLine;   	/* Stores information lines from the text user interface file in an array. */
static int *used;		/* Indicates if a line has been used, to make sure inputs are not ignored. */
static int no_tui_lines;		/* Number of information lines. */

static char *tuiFileName, *InputDirectory, *OutputDirectory; 


void check1(int x);
void check2(int x);
void check8(double x);


struct line_list {
	char *text;
	struct line_list *next;
};


/* End of declarations. */


static char* FindItem(char *header, int mandatory){
	int n,k;

	for(n=0;n<no_tui_lines;n++){
		for(k=0;header[k]==tuiLine[n][k] && header[k]!='\0';k++);
		if(header[k]=='\0'){
			used[n]=TRUE;
			return(&tuiLine[n][k]);
		}
	}
	if (mandatory){
		InputWarning("Could not find mandatory header '%s' in text user interface file.", header);
		tuiClose(tuiFileName);
		ExitMessage("Must exit.", header);
	}
	/* else */
	return(NULL);
}

void tuiGetInt(char *header, int mandatory, int *value){
	char *input, ch;
	int count;

	if((input=FindItem(header,mandatory))==NULL) return;

	count = sscanf(input, " %d %c", value, &ch);
	if( count < 1)
		ExitMessage("header '%s' in meta data followed by '%s' can not be converted "
			"to an integer.", header, input);
	if( count > 1 )	/* check for additional non-blank text in the line. */
		InputWarning("header '%s' in meta data followed by unused data \n'%s'\n", header, input);		
}


void tuiGetDouble(char *header, int mandatory, double *value){
	char *input, ch;
	int count;

	if((input=FindItem(header,mandatory))==NULL) return;
	count = sscanf(input, " %lf %c", value, &ch);
	if( count < 1)
		ExitMessage("header '%s' in meta data followed by '%s' can not be converted "
			"to a number.", header, input);
	if( count > 1 )	/* check for additional non-blank text in the line. */
		InputWarning("header '%s' in meta data followed by unused data \n'%s'\n", header, input);		
}


void tuiGetDoubleVector(char *header, int size, int mandatory, double *vector){
	char *input, ch;
	int i;

	if((input=FindItem(header,mandatory))==NULL) return;
	for(i=0; i<size; i++) {
		if( sscanf( input, " %lf", &(vector[i])) != 1 )
			ExitMessage("Header '%s' in text user interface, %d elements expected, %d elements found, followed by '%s' "
				"that can not be converted to a number.", header, size, i, input);
		strtod(input, &input); /* Skip to the next number. */
	}
	if( sscanf(input, " %c", &ch) == 1 )		/* Check for additional non-blank text in the line. */
		InputWarning("Header '%s' in text user interface followed by unused data '%s'.", header, input);		
}


void check1(int x){
	check2(6524663+x);
}

void tuiGetString(char *header, int mandatory, char **string){
	char *input, ch;
	int count;

	if((input=FindItem(header,mandatory))==NULL) {
		(*string) = (char *) NULL;
		return;
	}
	(*string) = (char *) calloc(strlen(input)+1, sizeof(char));
	count = sscanf(input, " %s %c", (*string), &ch);
	if(count < 1)
		ExitMessage("Header '%s' in text user interface is not followed by any text.", header);
	if( count > 1 )		/* Check for additional non-blank text in the line. */
		InputWarning("Header '%s' in text user interface followed by unused data '%s'.", header, input);		
}


void tuiGetStringVector(char *header, int size, int mandatory, char **string){
	char *input, *buffer, ch;
	int i, length;

	if((input=FindItem(header,mandatory))==NULL) return;
	buffer = (char *) calloc(strlen(input)+1, sizeof(char));
	for(i=0; i<size; i++) {
		while( isspace(*input)) input++;
		if( sscanf( input, "%s", buffer) != 1 )
			ExitMessage("Header '%s' in text user interface, %d elements expected, %d elements found, followed by '%s' "
				"that does not contain a string.", header, size, i, input);
		length = (int) strlen(buffer);
		string[i]=(char *) calloc(length+1, sizeof(char));
		strcpy( string[i], buffer);
		input+=length;		/* Skip to the next string. */
printf("%s ; %s ;\n", string[i], input);
	}
	if( sscanf(input, " %c", &ch) == 1 )		/* Check for additional non-blank text in the line. */
		InputWarning("Header '%s' in text user interface followed by unused data '%s'.", header, input);		
	free(buffer);
}



void check8(double x){
/*	if(x>6523926)
		ExitMessage("This license is limited to networks with 1,000,000 links");*/
}


void tuiGetInputFileName(char *header, int mandatory, char **Name){
	char *input, *buffer, ch;
	int count;

	if((input=FindItem(header,mandatory))==NULL) {
		(*Name) = (char *) NULL;
		return;
	}

	buffer = (char *) calloc(strlen(input)+1, sizeof(char));
	count = sscanf(input, " %s %c", buffer, &ch);
	if(count < 1)
		ExitMessage("Header '%s' in text user interface is not followed by a file name.", header);
	if( count > 1 )		/* Check for additional non-blank text in the line. */
		InputWarning("Header '%s' in text user interface followed by unused data '%s'.", header, input);		
	if(buffer[0]=='-') {
		if(mandatory){
			InputWarning("Invalid mandatory input file name '%s' for '%s' in text user interface file.", buffer, header);
			tuiClose(tuiFileName);
			ExitMessage("Must exit.", header);
		}
		else {
			(*Name) = (char *) NULL;
			return;
		}
	}

	(*Name) = (char *) calloc(strlen(InputDirectory)+strlen(buffer)+1, sizeof(char));
	sprintf((*Name), "%s%s", InputDirectory, buffer);
}

void tuiGetOutputFileName(char *header, int mandatory, char **Name){
	char *input, *buffer, ch;
	int count;

	if((input=FindItem(header,mandatory))==NULL) {
		(*Name) = (char *) NULL;
		return;
	}

	buffer = (char *) calloc(strlen(input)+1, sizeof(char));
	count = sscanf(input, " %s %c", buffer, &ch);
	if(count < 1)
		ExitMessage("Header '%s' in text user interface is not followed by a file name.", header);
	if( count > 1 )		/* Check for additional non-blank text in the line. */
		InputWarning("Header '%s' in text user interface followed by unused data '%s'.", header, input);		
	if(buffer[0]=='-') {
		if(mandatory){
			InputWarning("Invalid output file name '%s' for '%s' in text user interface file.", buffer, header);
			tuiClose(tuiFileName);
			ExitMessage("Must exit.", header);
		}
		else {
			(*Name) = (char *) NULL;
			return;
		}
	}

	(*Name) = (char *) calloc(strlen(OutputDirectory)+strlen(buffer)+1, sizeof(char));
	sprintf((*Name), "%s%s", OutputDirectory, buffer);
}



void check7(int x){
	check8( (double) 6.523926 * x);
}	


static char *ReadLine(FILE *fp){
	char buffer[1000];
	char *line;
	int n;

	fscanf( fp, "%1000[^\n^~] ", buffer );
	n = (int) strlen(buffer);
	line = (char *) calloc(n+1, sizeof(char));
	if(line==NULL) 
		ExitMessage("Can not allocate memory for line in text user interface.");		
	strcpy(line, buffer);

	return(line);
}
	

/* Read input data from text user interface file, stores temoprarily in a list of lines, then copy
the pointers to the global "line" array. */
void tuiInit(char *tuiFileName1){
	struct line_list *first_line, *current_line, *previous_line;
	FILE *tuiFile;
	int n;
	char *Line, *LogFileName;
	time_t t;
	struct tm *tt;

	t = time(NULL); /*time((time_t)0);*/
	tt = gmtime(&t);
#ifdef RELEASE
	check1(tt->tm_year);
#endif


	printf("Origin-Based Traffic Assignment for Infrastructure Networks (OBTAIN)\n");
	printf("This license is for academic use only !!!\n");
#ifdef RELEASE
	getchar();
#endif

	tuiFileName=tuiFileName1; 
	StatusMessage("General", "Reading text user interface file '%s'", tuiFileName);
	InputDirectory=OutputDirectory=NULL;

	tuiFile = FileOpen ( tuiFileName , "r" );

	first_line = (struct line_list *) malloc(sizeof(struct line_list));
	if(first_line==NULL) 
		ExitMessage("Can not allocate memory for list of lines in text user interface.");		

	current_line=first_line;
	n=0;
	SkipComments(tuiFile);
	while(!feof(tuiFile)){
		Line = ReadLine( tuiFile );
		if(strncmp(Line,"Input directory",15)==0) {
			InputDirectory = (char *) calloc(strlen(&(Line[15]))+1, sizeof(char));
			sscanf(&(Line[15]), " %s", InputDirectory);
			free(Line);
		}		
		else if(strncmp(Line,"Output directory",16)==0) {
			OutputDirectory = (char *) calloc(strlen(&(Line[16]))+1, sizeof(char));
			sscanf(&(Line[16]), " %s", OutputDirectory);
			free(Line);
		}
		else{ 	
			(*current_line).text=Line;	
			(*current_line).next = (struct line_list *) malloc(sizeof(struct line_list));
			if((*current_line).next==NULL) 
				ExitMessage("Can not allocate memory for list of lines in text user interface.");		
			current_line = (*current_line).next;
			n++;
		}
		SkipComments(tuiFile);
	}
	
	if(InputDirectory==NULL)
		ExitMessage("Can not find input directory name in text user interface.");		
	if(OutputDirectory==NULL)
		ExitMessage("Can not find output directory name in text user interface.");		

	no_tui_lines=n;	
	tuiLine = (char **) calloc( no_tui_lines, sizeof(char *));
	if(tuiLine==NULL) 
		ExitMessage("Can not allocate memory for array of lines in text user interface.");		
	used = (int *) calloc( no_tui_lines, sizeof(int));
	if(used==NULL) 
		ExitMessage("Can not allocate memory for array of line usage in text user interface.");		

	for(n=0, current_line=first_line; n<no_tui_lines && current_line!=NULL; n++){
		tuiLine[n]=(*current_line).text;
		previous_line=current_line;
		current_line = (*current_line).next;
		free(previous_line);
		used[n]=FALSE;
	}


	LogFileName=NULL;
	tuiGetOutputFileName( "Log file name", TRUE, &LogFileName);
	LogFile=FileOpen(LogFileName,"w");
	free(LogFileName);
	fprintf(LogFile, "This license is for academic use only !!!\n");
	fprintf(LogFile , "Started: %s\n",	Date_and_Time () );
	fprintf(LogFile , "Text user interface file: %s\n", tuiFileName );
}



void check2(int x){
	if(x>6524770)		
		ExitMessage("License has expired");
}



	
void tuiClose(char *tuiFileName1){
	int n;

	fprintf(LogFile , "Run Ended: %s \n\n", Date_and_Time() );
	fclose(LogFile);

	for(n=0;n<no_tui_lines;n++){
		if(!used[n])
			InputWarning("Text user interface file '%s' the following line was not used: \n%s", tuiFileName1, tuiLine[n]);
		free(tuiLine[n]);
	}
	free(tuiLine);
	free(used);

	printf("Origin-Based Traffic Assignment for Infrastructure Networks (OBTAIN)\n");
	printf("This license is for academic use only !!!\n");
#ifdef RELEASE
	getchar();
#endif

}


	
