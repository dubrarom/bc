
#include "stdafx.h"

/* Multi-dimensional memory allcation and deallocation functions.
Allocation functions return a pointer to Array[1:dim1][1:dim2]...
(Therefore all the allocation is done using "dim+1".)
*/

#include <stdio.h>
#include <stdlib.h>

#include "message.h"
#include "md_alloc.h"

void *Alloc_1D(int dim1, size_t size) {
	void *Array;

	Array = ( void *) calloc ( dim1+1 , size );
	if(Array==NULL) {
		ExitMessage("Can not allocate memory for single dimension array of size %d. \n",
				dim1);
	}
	return Array;
}

void **Alloc_2D(int dim1, int dim2, size_t size) {
	void **Array;
	int i;

	Array = ( void **) calloc ( dim1+1 , sizeof(void *) );
	if(Array==NULL) {
		ExitMessage("Can not allocate memory for two-dimensions array of size %d by %d. \n",
				dim1, dim2);
	}
	for ( i = 1 ; i <=dim1 ; i ++) {
		Array[i] = (void *) calloc ( dim2+1 , size ) ;
		if(Array[i]==NULL) {
			ExitMessage("Can not allocate memory for two-dimensions array of size %d by %d. \n",
					dim1, dim2);
		}
	}
	return (Array);
}


void ***Alloc_3D(int dim1, int dim2, int dim3, size_t size) {
	void ***Array;
	int i,j;

	Array = ( void ***) calloc ( dim1+1 , sizeof(void **) );
	if(Array==NULL) {
		ExitMessage("Can not allocate memory for three-dimensions array of size %d by %d by %d. \n",
				dim1, dim2, dim3);
	}
	for ( i = 1 ; i <=dim1 ; i ++) {
		Array[i] = (void **) calloc ( dim2+1 , sizeof(void *) );
		if(Array[i]==NULL) {
			ExitMessage("Can not allocate memory for three-dimensions array of size %d by %d by %d. \n",
					dim1, dim2, dim3);
		}
		for ( j = 1 ; j <=dim2 ; j ++) {
			Array[i][j] = (void *) calloc ( dim3+1 , size );
			if(Array[i][j]==NULL) {
				ExitMessage("Can not allocate memory for three-dimensions array of size %d by %d by %d. \n",
						dim1, dim2, dim3);
			}
		}
	}
	return (Array);
}


void ****Alloc_4D(int dim1, int dim2, int dim3, int dim4, size_t size) {
	void ****Array;
	int i,j,k;

	Array = ( void ****) calloc ( dim1+1 , sizeof(void ***) );
	if(Array==NULL) {
		ExitMessage("Can not allocate memory for four-dimensions array of size %d by %d by %d by %d. \n",
				dim1, dim2, dim3, dim4);
	}
	for ( i = 1 ; i <=dim1 ; i ++) {
		Array[i] = (void ***) calloc ( dim2+1 , sizeof(void **) );
		if(Array[i]==NULL) {
			ExitMessage("Can not allocate memory for four-dimensions array of size %d by %d by %d by %d. \n",
					dim1, dim2, dim3, dim4);
		}
		for ( j = 1 ; j <=dim2 ; j ++) {
			Array[i][j] = (void **) calloc ( dim3+1 , sizeof(void *) );
			if(Array[i][j]==NULL) {
				ExitMessage("Can not allocate memory for four-dimensions array of size %d by %d by %d by %d. \n",
						dim1, dim2, dim3, dim4);
			}
			for ( k = 1 ; k <=dim3 ; k ++) {
				Array[i][j][k] = (void *) calloc ( dim4+1 , size );
				if(Array[i][j][k]==NULL) {
					ExitMessage("Can not allocate memory for four-dimensions array of size %d by %d by %d by %d. \n",
							dim1, dim2, dim3, dim4);
				}
			}
		}
	}
	return (Array);
}




void Free_2D(void **Array, int dim1, int dim2) {
	int i;
	void *p;

	for ( i = 1 ; i <=dim1 ; i ++) {
		p = Array[i];
		free(p);
	}
	free(Array);
}

void Free_3D(void ***Array, int dim1, int dim2, int dim3) {
	int i,j;
	void *p, **pp;

	for ( i = 1 ; i <=dim1 ; i ++) {
		for ( j = 1 ; j <=dim2 ; j ++) {
			p=Array[i][j];
			free(p);
		}
		pp=Array[i];
		free(pp);
	}
	free(Array);
}


void Free_4D(void ****Array, int dim1, int dim2, int dim3, int dim4) {
	int i,j,k;
	void *p, **pp, ***ppp;

	for ( i = 1 ; i <=dim1 ; i ++) {
		for ( j = 1 ; j <=dim2 ; j ++) {
			for ( k = 1 ; k <=dim3 ; k ++) {
				p=Array[i][j][k];
				free(p);
			}
			pp=Array[i][j];
			free(pp);
		}
		ppp=Array[i];
		free(ppp);
	}
	free(Array);
}













