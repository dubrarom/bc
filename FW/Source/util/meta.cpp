
#include "stdafx.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* strlen, strcmp */

#include "my_util.h"
#include "message.h"
#include "meta.h"

struct meta_list {
	char *title;
	char *value;
	struct meta_list *next;
};


#define MAX_META_ITEMS 100



static char* FindMetaItem(char *title, int mandatory, struct meta_struct meta_data){
	int n;

	for(n=0;n<meta_data.no_items;n++){
		if(strcmp(meta_data.title[n],title)==0)
			return(meta_data.value[n]);
	}
	if (mandatory)
		ExitMessage("Could not find title '%s' in meta data of file %s.", title, meta_data.FileName); 
	/* else */
	return(NULL);
}

void GetMetaInt(char *title, int mandatory, int *i, struct meta_struct meta_data){
	char *value, ch;
	int count;

	if((value=FindMetaItem(title,mandatory,meta_data))==NULL) return;
	count = sscanf(value, " %d %c", i, &ch);
	if( count < 1)
		ExitMessage("title '%s' in meta data of file %s followed by '%s' can not be converted "
			"to an integer.", title, meta_data.FileName, value);
	if( count > 1 )	/* check for additional non-blank text in the line. */
		InputWarning("title '%s' in meta data of file %s followed by unused data \n'%s'\n", title, meta_data.FileName, value);		
}

void GetMetaDouble(char *title, int mandatory, double *d, struct meta_struct meta_data){
	char *value, ch;
	int count;

	if((value=FindMetaItem(title,mandatory,meta_data))==NULL) return;
	count = sscanf(value, " %lf %c", d, &ch);
	if( count < 1)
		ExitMessage("title '%s' in meta data of file %s followed by '%s' can not be converted "
			"to a number.", title, meta_data.FileName, value);
	if( count > 1 )	/* check for additional non-blank text in the line. */
		InputWarning("title '%s' in meta data of file %s followed by unused data \n'%s'\n", title, meta_data.FileName, value);		
}


void GetMetaString(char *title, int mandatory, char **string, struct meta_struct meta_data){
	char *value, q, buffer[1001];
	int count;

	if((value=FindMetaItem(title,mandatory,meta_data))==NULL) return;
	(*string) = (char *) calloc(strlen(value), sizeof(char));
	count = sscanf(value, " \"%[^\"]%[\"] %1000c", (*string), &q, buffer) ;	/* Get the string in quotation marks. */
	if(count < 2)
		ExitMessage("title '%s' in meta data of file '%s' followed by '%s' is not a \"double quoted\" string.", title, meta_data.FileName, value);
	if(count > 2)
		InputWarning("title '%s' in meta data of file '%s' followed by '%s' and by unused data \n'%s'\n", title, meta_data.FileName, (*string), buffer);		
}





static char *ReadMetaTitle(FILE *fp){
	char buffer[1010];
	char *title;
	int n;

	fscanf( fp, "%*[^<]" );	 			/* Ignore everything up to the first '<' symbol,*/
	n = fscanf( fp, "<%1000[^>]>", buffer );	/* and store the text between <> in buffer. */
	n = (int) strlen(buffer);
	title = (char *) calloc(n+1, sizeof(char));
	if(title==NULL) 
		ExitMessage("Can not allocate memory for meta data title of %d characters", n);		
	strncpy(title, buffer, n);
	title[n]='\0';

	return(title);
}

static char *ReadMetaValue(FILE *fp){
	char buffer[10010];
	char *value;
	int n;

	fscanf( fp, "%10000[^<]", buffer ); 	/* Read up to the next meta title, which starts with '<' */
	n = (int) strlen(buffer);
	value = (char *) calloc(n+1, sizeof(char));
	if(value==NULL) 
		ExitMessage("Can not allocate memory for meta data title of %d characters", n);		
	strncpy(value, buffer, n);
	value[n]='\0';

	return(value);
}



void ReadMeta(FILE *fp, struct meta_struct *meta_data) {
	struct meta_list *first_item, *current_item, *previous_item;
	int n, EndOfMeta=FALSE;

	first_item = (struct meta_list *) malloc(sizeof(struct meta_list));
	if(first_item==NULL) 
		ExitMessage("Can not allocate memory for list of meta data.");		

	current_item=first_item;
	n=0;
	fscanf( fp, "" );
	while( !feof(fp) && n<MAX_META_ITEMS){
		(*current_item).title = ReadMetaTitle( fp );
		if(strcmp((*current_item).title, END_OF_META)==0) {
			EndOfMeta=TRUE;
			break;
		}
		(*current_item).value = ReadMetaValue( fp );
		(*current_item).next = (struct meta_list *) malloc(sizeof(struct meta_list));
		if((*current_item).next==NULL) 
			ExitMessage("Can not allocate memory for list of meta data.");		
		current_item = (*current_item).next;
		n++;
	}
	if(n==MAX_META_ITEMS)
		ExitMessage("Input file '%s' contains more meta items then the limit (%d) \n", (*meta_data).FileName, MAX_META_ITEMS);  
	if(!EndOfMeta)
		ExitMessage("Did not find <%s> in file '%s' \n", END_OF_META, (*meta_data).FileName);  

	(*meta_data).no_items=n;	
	if(n>0){
		(*meta_data).title = (char **) calloc( (*meta_data).no_items, sizeof(char *));
		if((*meta_data).title==NULL) 
			ExitMessage("Can not allocate memory for array of %d meta data titles.", (*meta_data).no_items);		
		(*meta_data).value = (char **) calloc( (*meta_data).no_items, sizeof(char *));
		if((*meta_data).value==NULL) 
			ExitMessage("Can not allocate memory for array of %d meta data values.", (*meta_data).no_items);		

		for(n=0, current_item=first_item; n<(*meta_data).no_items && current_item!=NULL; n++){
			(*meta_data).title[n]=(*current_item).title;
			(*meta_data).value[n]=(*current_item).value;
			previous_item=current_item;
			current_item = (*current_item).next;
			free(previous_item);
		}
	}
}


void FreeMeta( struct meta_struct meta_data ) {
	int n;

	for(n=0; n<meta_data.no_items; n++){
		free(meta_data.title[n]);
		free(meta_data.value[n]);
	}
}


