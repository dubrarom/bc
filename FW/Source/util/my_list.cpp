
#include "stdafx.h"

/* List header file. */

#include <stdlib.h>
#include <stdio.h>

#include "os.h"
#include "my_list.h"

static struct list_item *NewListItem(int value){
	struct list_item *new_item;

	new_item=(struct list_item *) malloc(sizeof(struct list_item));
	if(new_item==NULL){
		printf("Error in memory allocation of list item. \n");
		exit(EXIT_FAILURE);
	}
	(*new_item).value=value;
	(*new_item).next_item=NULL;

	return(new_item);
}



/* Check if value is in list */
int ListFind(int Value, sorted_list list){
	struct list_item* item;

	for(item=list; item!=NULL && (*item).value < Value; item=(*item).next_item);
	if(item!=NULL && (*item).value == Value)
		return(TRUE);
	else
		return(FALSE);
}




/* Try to add a new item to the list. 
It is assumed that value is greater then the first value in the list. 
Return the list item that contains "value".*/

static struct list_item *InListAdd(int value, struct list_item *item){
	struct list_item *new_item, *remaining_list;

/* If there are no more items in the list, add the new value in the end. */
	if((*item).next_item==NULL){
		new_item=NewListItem(value);
		(*item).next_item=new_item;
		return(new_item);
	}

	remaining_list=(*item).next_item;

	if(value==(*remaining_list).value){
		return(remaining_list);
	}

	else if(value<(*remaining_list).value){
		new_item=NewListItem(value);
		(*new_item).next_item=remaining_list;
		(*item).next_item=new_item;
		return(new_item);
	}


	else{
		return(InListAdd(value, remaining_list));
	}

}


void ListAdd(int value, sorted_list *list){
	struct list_item *new_item;

	if(*list==NULL){
		*list=NewListItem(value);
	}
/* Adding the value in the beginning is a special case, all others are handled by recursion. */
	else if(value<(**list).value){
		new_item=NewListItem(value);
		(*new_item).next_item=(sorted_list) *list ;
		*list=new_item;
	}
	else if(value>(**list).value){
		InListAdd(value, *list);
	}

/* If the new value is equal to the first value then there is nothing to do. */

}


/* Try to remove an item from a list. 
It is assumed that value is greater then the first value in the list. 
If value is found in the list, return 1 and remove it;
otherwise return 0 and do not change the list. */

static int InListRemove(int value, struct list_item *item){
	struct list_item *remaining_list;

	if((*item).next_item==NULL)
		return(0);

	remaining_list=(*item).next_item;
	if(value==(*remaining_list).value){
		(*item).next_item = (*remaining_list).next_item;
		free(remaining_list);
		return(1);
	}
	else if(value<(*remaining_list).value) {
		return(0);
	}
	else {
		return(InListRemove(value, remaining_list));
	}
}

int ListRemove(int value, sorted_list *list){
	struct list_item *item_to_free;

	if(*list==NULL){
		return(0);
	}
	else if(value<(**list).value){
		return(0);
	}
	else if(value==(**list).value){
		item_to_free=(sorted_list) *list;
		*list=(**list).next_item;
		free(item_to_free);
		return(1);
	}
	else{		/* value > (**list).value */
		return(InListRemove(value, *list));
	}
}
	

static void InListFree(struct list_item* item) {

	if((*item).next_item!=NULL)
		InListFree( (*item).next_item );	
	free(item);
}

void ListFree(sorted_list list){
	
	if(list!=NULL)
		InListFree(list);
}



/* Creat a copy of a list. */
sorted_list ListCopy( sorted_list list){
	sorted_list new_list;
	struct list_item *item, *new_item;

/* If the first list is empty, return an empty list. */
	if(list==NULL)
		return(NULL);

	else{
		item=list;
		new_list=new_item=NewListItem((*item).value);
		item=(*item).next_item;
		while(item!=NULL){
			(*new_item).next_item=NewListItem((*item).value);
			new_item=(*new_item).next_item;
			item=(*item).next_item;
		}
		return(new_list);
	}
}


/* Merge list1 into list2. */
void ListMerge( sorted_list list1, sorted_list *list2){
	struct list_item *item1, *item2;

/* If the first list is empty, there is nothing to do. */
	if(list1==NULL)
		return;

/* If the second list empty, set it to a copy of list1. */
	if(*list2==NULL){
		*list2=ListCopy(list1);
	} 

	else{
/* InListAdd assumes that the first value in the list is lower then the new value. */
/* Therefore, if the first value in list1 is smaller, make it the first value in list2. Other values wil be larger. */
		if((*list1).value<(**list2).value){
			item2=NewListItem((*list1).value);
			(*item2).next_item=(sorted_list) *list2;
			*list2=item2;
			item1=(*list1).next_item;
		}
/* if the first value in list1 and list2 are equal, skip the first value in list1. Other values wil be larger. */
		else if((*list1).value==(**list2).value){
			item2=(sorted_list) *list2;
			item1=(*list1).next_item;
		}
/* Otherwise, if the first value in list1 is greater, start from the first value of list1. */
		else {	
			item2=(sorted_list) *list2;
			item1=list1;
		}


/* Add the remaining items of list1 one by one, continue always from the last item added. 
(previous items have lower values then the remaining values of list1.) */
		for(; item1!=NULL; item1=(*item1).next_item) 
			item2=InListAdd((*item1).value, item2);
	}
}




sorted_list ListIntersect(sorted_list list1, sorted_list list2){
	struct list_item *item1, *item2;
	sorted_list new_list=NULL;

	item1=list1; 
	item2=list2;
	while(item1!=NULL && item2!=NULL){
		if((*item1).value<(*item2).value)
			item1=(*item1).next_item;
		else if((*item2).value<(*item1).value)
			item2=(*item2).next_item;
		else {		/* (*item1).value==(*item2).value */
			ListAdd((*item1).value, &new_list);
			item1=(*item1).next_item;
			item2=(*item2).next_item;
		}
	}
	return(new_list);
}





/* Find all elements in list1 that do not show up in list2. */
sorted_list ListDifference(sorted_list list1, sorted_list list2){
	struct list_item *item1, *item2;
	sorted_list new_list=NULL;

	item1=list1; 
	item2=list2;
	while(item1!=NULL && item2!=NULL){
		if((*item1).value<(*item2).value){
			ListAdd((*item1).value, &new_list);
			item1=(*item1).next_item;
		}
		else if((*item2).value<(*item1).value){
			item2=(*item2).next_item;
		}
		else {			/* (*item1).value = (*item2).value */
			item1=(*item1).next_item;
			item2=(*item2).next_item;
		}		
	}

	while(item1!=NULL){ 		/* Add remaining items of list1, if there are any. */
		ListAdd((*item1).value, &new_list);
		item1=(*item1).next_item;
	}

	return(new_list);
}



int ListsAreEqual(sorted_list list1, sorted_list list2){
	struct list_item *item1, *item2;

	for(item1=list1, item2=list2; item1!=NULL; item1=(*item1).next_item){
		if(item2==NULL)
			return(0);
		else if ((*item1).value!=(*item2).value)
			return(0);
		item2=(*item2).next_item;
	}
	if(item2!=NULL)
		return(0);
	else
		return(1);
}


int ListSize(sorted_list list){
	struct list_item* item;
	int i;

	for(i=0, item=list; item!=NULL; i++, item=(*item).next_item) ;

	return(i);
}

void PrintList(sorted_list list, FILE *fp){
	struct list_item* item;
	int i;

	for(i=0, item=list; item!=NULL; i++, item=(*item).next_item) {
		fprintf(fp, "%i,",(*item).value);
	}
	fprintf(fp, "\n");

}



struct lex_node *LexAdd(struct lex_node* Lex, struct list_item *List){
	
	if(List==NULL){
		return(NULL);
	}
	if(Lex==NULL){
		Lex=(struct lex_node*) malloc(sizeof(struct lex_node));
		if(Lex==NULL){
			PrintList(List, stdout);
			printf("Can not allocate memory for lexicographic item");
			exit(EXIT_FAILURE);
		}
		Lex->value=List->value;
		Lex->next_item=LexAdd(NULL, List->next_item);		
		Lex->next_alternative=NULL;
	}
	else if(Lex->value==List->value){
		Lex->next_item=LexAdd(Lex->next_item, List->next_item);
	}
	else{
		Lex->next_alternative=LexAdd(Lex->next_alternative, List);
	}

	return(Lex);
}


int LexInclude(struct lex_node* Lex, struct list_item *List){
	if(List==NULL){
		return(TRUE);
	}
	else if(Lex==NULL){
		return(FALSE);
	}
	else if(Lex->value==List->value){
		return(LexInclude(Lex->next_item, List->next_item));
	}
	else{
		return(LexInclude(Lex->next_alternative, List));
	}
}



void LexPrint(struct lex_node* Lex, sorted_list List, FILE *fp){

	if(Lex==NULL)
		return;
	ListAdd(Lex->value, &List); 
	if(Lex->next_item==NULL){
		PrintList(List, fp);
	}
	else{
		LexPrint(Lex->next_item, List, fp);
	}
	ListRemove(Lex->value, &List);
	LexPrint(Lex->next_alternative, List, fp);
}



void LexFree(struct lex_node* Lex){
	if(Lex==NULL)
		return;
	LexFree(Lex->next_item);
	LexFree(Lex->next_alternative);
	free(Lex);
}

