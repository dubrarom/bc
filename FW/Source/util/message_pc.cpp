
#include "stdafx.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h> /* vprintf ... */
#include <string.h> /* strcmp */

#include "my_util.h"
#include "message.h"


void StatusMessage( const char *group, const char *format, ...) {
	double new_time;
	va_list ap;
	va_start(ap, format);

	new_time = my_clock(); /* Check the clock anyway, to minimize wrap errors. */
	if(strcmp(group, "General")==0) {
		printf("Clock %10.0f : ", new_time );
		vprintf( format, ap );
		printf("\n");
	}

	va_end(ap);
}

void Warning(const char *format, ...) {
	va_list ap;

	printf("Warning: ");
	va_start(ap, format);
	vprintf( format, ap );
	va_end(ap);

	printf("\n");
}



void ExitMessage( const char *format, ...) {
	va_list ap;

	va_start(ap, format);
	vprintf( format, ap );
	va_end(ap);

	printf("\n");

	getchar();

	exit(EXIT_FAILURE);	
}

void InputWarning(const char *format, ...) {
	static int count=0;
	va_list ap;

	printf("Input warning %d: ", ++count);
	va_start(ap, format);
	vprintf( format, ap );
	va_end(ap);

	printf("\n");
}


#define BUFFERSIZE 100


void FatalInputError(FILE *fp, const char *FileName, const char *message_format, ...){
	char Buffer[BUFFERSIZE];
	va_list ap;

	va_start(ap, message_format);
	my_gets(fp, Buffer, BUFFERSIZE);

	printf("Fatal error in reading input file %s \n", FileName);
	vprintf( message_format, ap );
	printf("\nFollowed by \n");
	printf("%s \n", Buffer);

	va_end(ap);

	getchar();

	exit(EXIT_FAILURE);
}


