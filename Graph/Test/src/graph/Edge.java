package graph;

public class Edge {
    Point start;
    Point end;

    public Edge(Point start, Point end) {
        this.start = start;
        this.end = end;
    }
//properties:
    //int length;
    //double capacity;
    //boolean bothDirections;
}