package graph;



import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Alg {
     private List<Point> graphPoints;

    public Alg() {
        graphPoints= new ArrayList<Point>();
    }

    public List<Point> readGraphPoints(String filename){
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(filename));

            reader.readLine(); //first row
            String line = reader.readLine();

            while (line != null) {
                String[] l = line.split("\t");
                graphPoints.add(new Point(Integer.parseInt(l[1])/1000,
                        Integer.parseInt(l[2])/1000,
                        Integer.parseInt(l[0])));
                // read next line
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return graphPoints;
    }

    public void readEdges(String filename){
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(filename));

            reader.readLine(); //first row
            String line= reader.readLine();;
            for (int i = 0; i < 5; i++) {
                 line = reader.readLine();
            }

            while (line != null) {
                String[] l = line.split("\t");
                for(Point p : graphPoints){
                    if(p.number == Integer.parseInt(l[1])){
                        Optional<Point> point = graphPoints.stream().filter(item -> item.number == Integer.parseInt(l[2])).findFirst();
                        if(point.isPresent()){
                            p.addEdge(point.get());
                        }

                    }
                }
                // read next line
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        Alg a = new Alg();

        System.out.println("Working Directory = " +
                System.getProperty("user.dir"));

        List<Point> graphPoints = a.readGraphPoints("Test/falls/SiouxFalls_node.tntp");
        a.readEdges("Test/falls/SiouxFalls_net.tntp");

        GraphWindow g = new GraphWindow();

        g.makeGraph(graphPoints);
        g.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        g.setSize(400, 320);
        g.setVisible(true);

    }
}


