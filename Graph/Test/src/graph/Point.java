package graph;


import java.util.ArrayList;
import java.util.List;

public class Point {
    public int x;
    public int y;
    public int number;
    public Object vertex;

    public List<Edge> edges;

    public Point(int x, int y, int num) {
        this.x = x;
        this.y = y;
        number = num;
        edges = new ArrayList<Edge>();
    }

    public void addEdge(Point other){
        edges.add(new Edge(this, other));
    }
    public void setVertex(Object v){
        vertex = v;
    }
}