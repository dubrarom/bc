package graph;


import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;

import javax.swing.*;
import java.util.List;

public class GraphWindow extends JFrame {
    public GraphWindow() {
        super("Graph");
    }

    public void makeGraph(List<Point> points){
        mxGraph graph = new mxGraph();
        Object parent = graph.getDefaultParent();

        graph.getModel().beginUpdate();
        try
        {
//            Object start = graph.insertVertex(parent, null, "Hello,", 20, 20, 80, 30);
//            Object end = graph.insertVertex(parent, null, "World!", 200, 150, 80, 30);
//            Object e1 = graph.insertEdge(parent, null, "", start, end);

            for(Point p : points){
                p.setVertex(graph.insertVertex(parent, null, "x: "+p.x+"\ny: "+p.y, p.x, p.y, 10, 10));

            }
            for(Point p : points){
                for (Edge e : p.edges){
                    graph.insertEdge(parent, null, "Edge", e.start.vertex, e.end.vertex);
                }
            }

//            Object start = graph.insertVertex(parent, null, "Hello", 20, 20, 80, 30);

        }
        finally
        {
            graph.getModel().endUpdate();
        }

        mxGraphComponent graphComponent = new mxGraphComponent(graph);
        getContentPane().add(graphComponent);
    }
}